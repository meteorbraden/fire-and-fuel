<?php 
		
	$baseUrl = 'https://b1bd049a97732e0ff9c2e831682e4e33:05cb21afbf12fa7aa2e1a9425ee8f34f@fire-and-fuel-clothing.myshopify.com/admin/';
	set_time_limit(0); 

	$headers = get_headers($baseUrl.'shop.json', 1); //this will fetch the headers from Shopify and the second parameter specifies the function to parse it into an array structure
	$apiLimit = (int) str_replace('/500', '', $headers['HTTP_X_SHOPIFY_SHOP_API_CALL_LIMIT']); 
	$response = json_decode(file_get_contents($baseUrl.'products/count.json')); //request it from the shopify api
	$productCount = $response->count; //get the count parameter
	$pages = ceil($productCount/250); //get the amount of pages with max 250 products per page
?>			

<!doctype html>
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en"><!--<![endif]-->
    <head>
        <title>Shop</title>
		<!--meta info-->
		<meta charset="utf-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
		<meta name="author" content="">
		<meta name="keywords" content="">
		<meta name="description" content="">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.css" type="text/css">
        <!--[if !IE 9]><!-->
		<link rel="stylesheet" href="css/effect.css" type="text/css">
		<link rel="stylesheet" href="css/animation.css" type="text/css">
		<!--<![endif]-->
        <link rel="stylesheet" href="css/masterslider.css" type="text/css">
        <link rel="stylesheet" type="text/css" media="all" href="css/owl.carousel.css">
        <link rel="stylesheet" type="text/css" media="all" href="css/owl.transitions.css">
        <link rel="stylesheet" href="css/jquery.custombox.css" type="text/css">
		<link rel="stylesheet" href="css/color.css" type="text/css">
		
		
		

    </head> 
    <body id="page-top" class="" data-offset="90" data-target=".navigation" data-spy="scroll">
        <div class="wrapper hide-main-content"> 
            <section class="page page-product">
             	<?php include 'includes/nav.php'; ?>
             	
        
		
		<?php 
	
		// Get products 
	    // --------------------------------
	    
			$url = $_SERVER['REQUEST_URI'];
		        	
			if (strpos($url, "?product=")!==false){
				$product_id = explode('=', $url);
				$id = $product_id[1];
		
					$response = json_decode(file_get_contents($baseUrl.'products.json'));
					foreach ($response->products as $product) {
						
						if ($product->handle == $id){
							$result = json_decode(json_encode($product),true);
						    error_log( print_R($result,TRUE) );				

							
						}
					}
				
		?>
		
		
		<!--Banner-->
                    <section class="page-heading">
                        <div class="title-slide">
                            <div class="container">
                                <div class="banner-content">									
                                    <div class="page-title">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
					
					<?php include 'includes/social-icons.php'; ?>

                    <!--End Banner-->
                    <div class="page-content">					
                        <!-- Breadcrumbs -->
                        <div class="breadcrumbs">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-9">
                                        <ul>
                                            <li class="home"><a href="/"><i class="fa fa-home"></i> Home</a></li>
                                            <li><span>//</span></li>
                                            <li class="home"><a href="/shop">Shop</a></li>
                                            <li><span>//</span></li>
                                            <li class="category-2"><?php echo $result['title']; ?></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Breadcrumbs -->
                        <div class="product-detail" style="background: #AD3847;">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="product-view">
                                            <div class="product-essential col-md-12" style="background-color: white; color: black;">
                                                <div class="product-img-box col-md-4">
                                                    <a href="#"><img alt="" src="<?php echo $result['image']['src']; ?>"></a>
<!--
                                                    <div class="more-views">
                                                        <div id="owl-demo" class="owl-carousel owl-theme">
                                                            <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                            <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                            <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                            <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                            <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                            <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                            <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                            <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                            <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                            <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                        </div>
                                                        <!--  <a class="jcarousel-control-prev" href="#"><i class="fa fa-caret-left"></i></a>
                                                         <a class="jcarousel-control-next" href="#"><i class="fa fa-caret-right"></i></a> -->
<!--
                                                        <div class="customNavigation">
                                                            <a class="btn prev"><i class="fa fa-caret-left"></i></a>
                                                            <a class="btn next"><i class="fa fa-caret-right"></i></a>
                                                        </div>
                                                    </div>
-->
                                                </div>
                                                <div class="product-shop col-md-8">
                                                    <div class="product-name">
                                                        <h1><?php echo $result['title']; ?></h1>
                                                    </div>
                                                    <div class="meta-box">
                                                        <div class="price-box">																										
														<script type="text/javascript">!function(){function b(){var b=document.createElement("script");b.async=!0,b.src=a,(document.getElementsByTagName("head")[0]||document.getElementsByTagName("body")[0]).appendChild(b),b.onload=c}function c(){var a=ShopifyBuy.buildClient({domain:"fire-and-fuel-clothing.myshopify.com",apiKey:"31e30886f50ac148ccae7d98323da09b",appId:"6"});ShopifyBuy.UI.onReady(a).then(function(a){a.createComponent("product",{id:[<?php echo $result['id']?>],node:document.getElementById("<?php echo $result['handle']?>"),moneyFormat:"%24%7B%7Bamount%7D%7D",options:{product:{variantId:"all",width:"240px",contents:{img:!1,title:!1,variantTitle:!1,price:!1,description:!1,buttonWithQuantity:!1,quantity:!1},styles:{product:{"text-align":"left","@media (min-width: 601px)":{"max-width":"calc(25% - 20px)","margin-left":"20px","margin-bottom":"50px"}},button:{"background-color":"#ff4f00",":hover":{"background-color":"#e64700"},":focus":{"background-color":"#e64700"}}}},cart:{contents:{button:!0},styles:{button:{"background-color":"#ff4f00",":hover":{"background-color":"#e64700"},":focus":{"background-color":"#e64700"}},footer:{"background-color":"#ffffff"}}},modalProduct:{contents:{variantTitle:!1,buttonWithQuantity:!0,button:!1,quantity:!1},styles:{product:{"@media (min-width: 601px)":{"max-width":"100%","margin-left":"0px","margin-bottom":"0px"}},button:{"background-color":"#ff4f00",":hover":{"background-color":"#e64700"},":focus":{"background-color":"#e64700"}}}},toggle:{styles:{toggle:{"background-color":"#ff4f00",":hover":{"background-color":"#e64700"},":focus":{"background-color":"#e64700"}},count:{color:"#ffffff",":hover":{color:"#ffffff"}},iconPath:{fill:"#ffffff"}}},productSet:{styles:{products:{"@media (min-width: 601px)":{"margin-left":"-20px"}}}}}})})}var a="https://sdks.shopifycdn.com/buy-button/latest/buy-button-storefront.min.js";window.ShopifyBuy&&window.ShopifyBuy.UI?c():b()}();</script>

                                                        </div>                                                    
                                                    </div>
                                                    <div class="clearfix"><h3>$<?php echo $result['variants'][0]['price']; ?></h3></div>
                                                    <div class="tags-list">
                                                        <label>Tags</label><span>:</span>
                                                        <ul>
                                                            <li><?php echo $result['tags']; ?></li>
                                                        </ul>
                                                    </div>
                                                                                                                                                                          
<!--
													<div data-embed_type="product" data-shop="fire-and-fuel-apparrel.myshopify.com" data-product_name="<?php echo $result['title']; ?>" data-product_handle="<?php echo $result['handle']; ?>" data-has_image="false" data-display_size="compact" data-redirect_to="cart" data-buy_button_text="Add to cart" data-buy_button_out_of_stock_text="Out of Stock" data-buy_button_product_unavailable_text="Unavailable" data-button_background_color="ad3847" data-button_text_color="ffffff" data-product_modal="false" data-product_title_color="000000" data-next_page_button_text="Next page"></div>
														<script type="text/javascript">
														document.getElementById('ShopifyEmbedScript') || document.write('<script type="text/javascript" src="https://widgets.shopifyapps.com/assets/widgets/embed/client.js" id="ShopifyEmbedScript"><\/script>');
														</script>
														<noscript language="html"><a href="https://fire-and-fuel-apparrel.myshopify.com/cart/<?php echo $result['variants'][0]['id']; ?>:1" target="_blank">Buy 555 Fitness Tank Top</a></noscript>
                                                 
-->
                                                    </div>
                                                    <div class="tags-list">
                                                        <label>Tags</label><span>:</span>
                                                        <ul>
                                                            <li><?php echo $result['tags']; ?></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="tabs" class="product-collateral">
                                                <ul>
                                                    <li><a href="#tabs-1">Description</a></li>
<!--                                                     <li><a href="#tabs-2">Reviews</a></li> -->
                                                </ul>
                                                <div id="tabs-1" class="box-collateral">
                                                    <?php echo $result['body_html']; ?>
                                                </div>
                                            </div>                                      
                                    	</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>
             	
             	<?php }     
    
        
    // All products 
    // --------------------------------
    else
    {
	
	?>				
					<!--Banner-->
					<section class="page-heading">
						<div class="title-slide">
							<div class="container">
								<div class="banner-content">									
									<div class="page-title">
										<h3>Shop</h3>
									</div>
								</div>
							</div>
						</div>
					</section>
					<!--End Banner-->
					<div class="page-content">					
						<!-- Breadcrumbs -->
						<div class="breadcrumbs">
							<div class="container">
								<div class="row">
									<div class="col-md-9">
										<ul>
											<li class="home"><a href="/"><i class="fa fa-home"></i> Home</a></li>
											<li><span>//</span></li>
											<li class="category-2"><a href="#">Shop</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<!-- End Breadcrumbs -->
						<div class="product-list">
							<div class="container">
								<div class="row">
									<div class="col-md-9">
										<div class="toolbar">
											<div class="sorter">
												<div class="sort-by">
													<select onchange="setLocation(this.value)">
														<option selected="selected" value="">Sort By</option>
														<option value="">Position</option>
														<option value="">Name</option>
														<option value="">Price</option>
													</select>
												</div>                                                
											</div>
										</div>
										<div class="row">
											
											<?php
						            
									           $response = json_decode(file_get_contents($baseUrl.'products.json')); 
											   foreach ($response->products as $product) {
												   $result = json_decode(json_encode($product),true);
												   error_log( print_R(count($result),TRUE) );				

													
											?>


											
											<div class="col-md-4 col-sm-6 col-xs-12">
												<div class="product-image-wrapper">
													<div class="product-content">																											
														<div class="product-image shop-products">
															<a href="/shop?product=<?php echo $result['handle']; ?>"><img class="product-img" alt="" src="<?php echo $result['image']['src']; ?>"></a>
														</div>
														<div class="info-products">
															<div class="product-list-name">
																<a href="product-detail.html"><?php echo $result['title']; ?></a>
																<div class="product-bottom"></div>
															</div>
															<div class="price-box">																										
																<span class="special-price"><?php echo $result['variants'][0]['price']; ?></span>
															</div>
															<div class="actions">
																<ul>																	
																	<li><a href="?product=<?php echo $result['handle']; ?>"><i class="fa fa-shopping-cart"></i></a></li>
																	<li><a href="#"><i class="fa fa-heart"></i></a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
											</div>

									
												<?php 
													
													$current_page++;
													}
												?>
											
											

									</div>
										<div class="clearfix"></div>
										
									</div>
									<div class="col-md-3">
					
										<div class="product-categories">
											<h3 class="title">Product categories</h3>
											<ul>
						
												<li><a href="/product-category/womens-fire-apparel">Womens</a></li>
												<li><a href="/product-category/firefighter-hats">Hats</a></li>
												<li><a href="/product-category/fitness-clothing">Fitness Apparel</a></li>
												<li><a href="/product-category/on-point-supplements">On Point Supplements</a></li>
												<li><a href="/product-category/kids">Kids</a></li>
												<li><a href="/product-category/gear">Gear</a></li>
												<li><a href="/product-category/accessories">Accessories</a></li>
											</ul>
										</div>
										<div class="product-popular">
											<h3 class="title">Popular products</h3>
											<ul>
												<li>
													<div class="product-image">
														<a href="#"><img alt="" src="images/popular-product.jpg"></a>
													</div>
													<div class="info-products">
														<div class="product-name">Lorem ipsum dolor sit amet consectetur</div>
														<div class="price-box">																										
															<span class="regular-price">$200.60</span>
														</div>
													</div>
												</li>												
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
                </div>
            </section>
            <?php include 'includes/social-icons.php'; ?>
            
            
        <?php } ?>
		
		
		<?php include 'includes/footer.php'; ?>
		
            <!--To Top-->
            <div id="copyright">
                <div class="container">

                    <div class="back-to-top"><a title="BACK_TO_TOP" href="#top"><i class="fa fa-chevron-up"></i></a></div>

                    <div class="clrDiv"></div>
                </div>
            </div>
            <!--End To Top-->
        </div>

        <script type="text/javascript" src="js/jquery-2.1.0.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui.js"></script>
        <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="js/waypoints.js"></script>
        <script type="text/javascript" src="js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="js/template.js"></script>
        <script type="text/javascript" src="js/masterslider.min.js"></script>	
        <script type="text/javascript" src="js/dropdown.js"></script>
        <script type="text/javascript" src="js/classie.js"></script>
        <script type="text/javascript" src="js/jquery.custombox.js"></script>
        <script type="text/javascript" src="js/main.js"></script>
        <script type="text/javascript">
            $(function() {
                $('.quickview').on('click', function(e) {
                    $.fn.custombox(this);
                    var owl = $("#owl-demo");
                    owl.owlCarousel({
                        items: 5,
                        itemsDesktop: [1000, 5],
                        itemsDesktopSmall: [900, 3],
                        itemsTablet: [600, 2],
                        itemsMobile: false,
						pagination: false
                    });
                    $(".next").click(function() {
                        owl.trigger('owl.next');
                    })
                    $(".prev").click(function() {
                        owl.trigger('owl.prev');
                    })
                    e.preventDefault();
                });
            });

            $('#price-filter').slider({
                range: true,
                min: 0,
                max: 999,
                values: [100, 700],
                slide: function(event, ui)
                {
                    $('#price-filter-value-1').text(ui.values[0]);
                    $('#price-filter-value-2').text(ui.values[1]);
                    var min = ui.values[0] / 999 * 90;
                    var max = ui.values[1] / 999 * 90;
                    $('.min-filter').css('left', min + '%');
                    $('.max-filter').css('left', max + '%');
                }
            });
            function increaseQty(id){
                var qtya = $('.qty-'+id).val();
                var qtyb = qtya * 1 + 1;
                $('.qty-'+id).val(qtyb);
            }
            function decreaseQty(id){
                var qtya = $('.qty-'+id).val();
                var qtyb = qtya * 1 - 1;
                if (qtyb < 1) {
                    qtyb = 1;
                }
                $('.qty-'+id).val(qtyb);
            }
        </script>
    </body>
</html>
