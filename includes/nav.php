   <!--Menu Mobile-->
			<div class="menu-wrap">
				<div class="main-menu">
					<h4 class="title-menu">Main menu</h4>
					<button class="close-button" id="close-button"><i class="fa fa-times"></i></button>
				</div>
				<ul class="nav-menu">
					<li class=" dropdown selected active">
						<a href="/" data-toggle="dropdown">Home<i class="icon-arrow"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" data-toggle="dropdown">Our Inspiration<i class="icon-arrow"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" data-toggle="dropdown">Shop<i class="icon-arrow"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" data-toggle="dropdown">Fitness Clothing<i class="icon-arrow"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" data-toggle="dropdown">Roll Call<i class="icon-arrow"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" data-toggle="dropdown">Gifts<i class="icon-arrow"></i></a>
					</li>
					<li><a href="#">Social</a></li>
					<li><a href="#">Contact us</a></li>
				</ul>
			</div>
			<!--Menu Mobile End-->
			<div class="content-wrapper">
				<!--Header-->				
				<header id="header" style="background: #242424;" class="header header-container">
					<div class="container">
						<div class="row">
							<div class="col-md-2 col-sm-3 col-xs-3 logo">
								<a href="/"><img style="height: 82px; margin-left: 55px;" src="../assets/images/logo.png" alt=""/></a>
							</div>
							<div class="col-md-10 nav-container">
								<nav class="megamenu collapse navbar-collapse bs-navbar-collapse navbar-right mainnav col-md-12" role="navigation">
									<ul class="nav-menu">
										<li class="selected"><a href="/">Home</a></li>
										<li>
											<a href="/events">About</a>
											<ul class="child-nav">
													<li><a href="/events">Events</a></li>
												</ul>
										<li>
											<a href="/shop">Shop</a>
											<ul class="child-nav">
												<li><a href="/product-category/american-fighter">Mens</a></li>
												<li><a href="/product-category/womens-fire-apparel">Womens</a></li>
												<li><a href="/product-category/firefighter-hats">Hats</a></li>
												<li><a href="/product-category/fitness-clothing">Fitness Apparel</a></li>
												<li><a href="/product-category/on-point-supplements">On Point Supplements</a></li>
												<li><a href="/product-category/kids">Kids</a></li>
												<li><a href="/product-category/">Gear</a></li>
											</ul>
										</li>
										<li><a href="/product-category/fitness-clothing">Fitness Clothing</a></li>
										<li><a href="/roll-call">Roll Call</a></li>
										<li><a href="/firefighter-gifts">Gifts</a></li>
										<li><a href="/contact">Contact</a></li>
										<div class="free-shipping-banner">
											<h1>Free Shipping in the US!</h1>
										</div>
									</ul>
								</nav>							
							</div>
							</div>
							<button class="menu-button" id="open-button"></button>
						</div>
					</div>
				</header>