<div class="product-related row">
                                                <div class="col-md-12">
                                                    <h3 class="title">Product Related</h3>
                                                </div>
                                                <div class="col-md-4 col-sm-6 col-xs-12">
                                                    <div class="product-image-wrapper">
                                                        <div class="product-content">
                                                            <div class="product-image">
                                                                <a href="#"><img alt="" src="images/newproducts/product-1.jpg"></a>
                                                            </div>
                                                            <div class="info-products">
																<div class="product-name" >
																	<a href="#">Horizon Andes</a>
																	<div class="product-bottom"></div>
																</div>
																<div class="price-box">																										
																	<span class="special-price">$200.60</span>
																	<span class="old-price">$227.96</span>
																</div>
																<div class="actions">
																	<ul>
																		<li><a href="#"><i class="fa fa-info"></i></a></li>
																		<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
																		<li><a href="#"><i class="fa fa-search"></i></a></li>
																		<li><a href="#"><i class="fa fa-star"></i></a></li>
																	</ul>
																</div>
															</div>
                                                        </div>
                                                        <a class="arrows quickview" href="#quickview"><i class="fa fa-arrows"></i></a>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-6 col-xs-12">
                                                    <div class="product-image-wrapper">
                                                        <div class="product-content">
                                                            <div class="product-image">
                                                                <a href="#"><img alt="" src="images/newproducts/product-2.jpg"></a>
                                                            </div>
                                                            <span class="new-label">new</span>
                                                            <div class="info-products">
																<div class="product-name" >
																	<a href="#">Octane Deluxe Console</a>
																	<div class="product-bottom"></div>
																</div>
																<div class="price-box">																										
																	<span class="special-price">$200.60</span>
																	<span class="old-price">$227.96</span>
																</div>
																<div class="actions">
																	<ul>
																		<li><a href="#"><i class="fa fa-info"></i></a></li>
																		<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
																		<li><a href="#"><i class="fa fa-search"></i></a></li>
																		<li><a href="#"><i class="fa fa-star"></i></a></li>
																	</ul>
																</div>
															</div>
                                                        </div>
                                                        <a class="arrows quickview" href="#quickview"><i class="fa fa-arrows"></i></a>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-6 col-xs-12">
                                                    <div class="product-image-wrapper">
                                                        <div class="product-content">
                                                            <div class="product-image">
                                                                <a href="#"><img alt="" src="images/newproducts/product-3.jpg"></a>
                                                            </div>
                                                            <div class="info-products">
																<div class="product-name" >
																	<a href="#">Precor Recumbent Cycle</a>
																	<div class="product-bottom"></div>
																</div>
																<div class="price-box">																										
																	<span class="special-price">$200.60</span>
																	<span class="old-price">$227.96</span>
																</div>
																<div class="actions">
																	<ul>
																		<li><a href="#"><i class="fa fa-info"></i></a></li>
																		<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
																		<li><a href="#"><i class="fa fa-search"></i></a></li>
																		<li><a href="#"><i class="fa fa-star"></i></a></li>
																	</ul>
																</div>
															</div>
                                                        </div>
                                                        <a class="arrows quickview" href="#quickview"><i class="fa fa-arrows"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="quickview" class="quickview-box container">
                                                <div class="row">
                                                    <div class="quickview-body col-md-9">
                                                        <div class="product-essential">
                                                            <button type="button" class="quickview-close" onclick="$.fn.custombox('close');">
                                                                <i class="fa fa-times"></i>
                                                            </button>
                                                            <div class="product-img-box col-md-6">
                                                                <a href="#"><img alt="" src="images/newproducts/product-detail.jpg"></a>
                                                                <div class="more-views">
                                                                    <div id="owl-demo-box" class="owl-carousel owl-theme">
                                                                        <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                                        <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                                        <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                                        <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                                        <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                                        <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                                        <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                                        <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                                        <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                                        <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                                    </div>
                                                                    <div class="customNavigation">
                                                                        <a class="btn prev"><i class="fa fa-caret-left"></i></a>
                                                                        <a class="btn next"><i class="fa fa-caret-right"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="product-shop col-md-6">
                                                                <div class="product-name">
                                                                    <h1>Horizon Andes</h1>
                                                                </div>
                                                                <div class="meta-box">
                                                                    <div class="price-box">																										
                                                                        <span class="special-price">$200.60</span>
                                                                        <span class="old-price">$227.96</span>
                                                                    </div>
                                                                    <div class="rating-box">
                                                                        <div class="rating">
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                <div class="short-description">
                                                                    To be in a good shape is very important nowadays because our way of life isn't so healthy and active. Most of us spend endless hours sitting by the computer and it gives negative results very fast. We think that fitness is a pleasant and useful activity. It helps to be more successful and disciplined.
                                                                </div>
                                                                <div class="add-to-box">
                                                                    <div class="add-to-cart">
                                                                        <input type="text" class="input-text qty-2" title="Qty" value="1" maxlength="12" id="qty-box" name="qty">
                                                                        <span class="increase-qty" onclick="increaseQty('2')"><i class="fa fa-sort-up"></i></span>
                                                                        <span class="decrease-qty" onclick="decreaseQty('2')"><i class="fa fa-sort-down"></i></span>
                                                                    </div>
                                                                    <button class="button btn-cart" title="Add to Cart" type="button">
                                                                        <em class="fa-icon"><i class="fa fa-shopping-cart"></i></em>
                                                                        <span>Add Cart</span>
                                                                    </button>
                                                                    <a class="link-wishlist"><i class="fa fa-heart"></i></a>
                                                                </div>
                                                                <div class="cat-list">
                                                                    <label>Categories</label><span>:</span>
                                                                    <ul>
                                                                        <li><a href="#">Dress,</a></li>
                                                                        <li><a href="#">Lorem</a></li>
                                                                    </ul>
                                                                </div>
                                                                <div class="tags-list">
                                                                    <label>Tags</label><span>:</span>
                                                                    <ul>
                                                                        <li><a href="#">Fitness</a></li>
                                                                        <li><a href="#">Lorem</a></li>
                                                                    </ul>
                                                                </div>
                                                                <div class="social-icon">
                                                                    <ul>
                                                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                                        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>