<!--Footer-->
		<footer class="page-footer">
			<section>
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-12 infomation">
							<div class="copy-right">
								<div class="footer-right">
									<div class="footer-title">
										<h4>Sitemap</h4>
										<ul class="footer-list">
											<li class="footer-links"><a href="/">Home</a></li>
											<li class="footer-links"><a href="#">Our Inspiration</li>
											<li class="footer-links"><a href="/shop">Shop</li>
											<li class="footer-links"><a href="/product-category/fitness-clothing">Fitness Clothing</li>
											<li class="footer-links"><a href="/roll-call">Roll Call</li>
											<li class="footer-links"><a href="/product-category/accessories">Gifts</li>
											<li class="footer-links"><a href="#">Social</li>
											<li class="footer-links"><a href="/contact">Contact</li>
										</ul>			
									</div>
									<div class="line1">Copyright &copy; 2016 Fire and Fuel Apparel</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12 location">							
							<div class="footer-title">
								<h4>Product Categories</h4>
								<ul class="footer-list">
									<li class="footer-links"><a href="/product-category/accessories">Accessories</li>
									<li class="footer-links"><a href="/product-category/fitness-clothing">Fitness Clothing</li>
									<li class="footer-links"><a href="/product-category/gear">Gear</li>
									<li class="footer-links"><a href="/product-category/firefighter-hats">Hats</li>
									<li class="footer-links"><a href="/product-category/kids">Kids</li>
									<li class="footer-links"><a href="/product-category/american-fighter">Mens</li>
									<li class="footer-links"><a href="/product-category/on-point-supplements">On Point Supplements</li>
								</ul>				
							</div>	
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12 send-mail">							
							<div class="fb-page" data-href="https://www.facebook.com/FireAndFuelApparel/?fref=ts" data-tabs="timeline" data-width="350" data-height="250" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/FireAndFuelApparel/?fref=ts" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/FireAndFuelApparel/?fref=ts">Fire And Fuel Apparel</a></blockquote></div>
						</div>						
					</div>
				</div>
			</section>
		</footer>
		<!--End Footer-->