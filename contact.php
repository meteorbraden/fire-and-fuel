<!doctype html>
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en"><!--<![endif]-->
<head>
<title>Contact Us</title>
<!--meta info-->
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
<meta name="author" content="">
<meta name="keywords" content="">
<meta name="description" content=""> 
<link rel="stylesheet" href="css/style.css" type="text/css">
<link rel="stylesheet" href="css/bootstrap.css" type="text/css">
<link rel="stylesheet" href="css/font-awesome.css" type="text/css">
<!--[if !IE 9]><!-->
<link rel="stylesheet" href="css/effect.css" type="text/css">
<link rel="stylesheet" href="css/animation.css" type="text/css">
<!--<![endif]-->
<link rel="stylesheet" href="css/color.css" type="text/css">
</head> 
<body id="page-top" class="" data-offset="90" data-target=".navigation" data-spy="scroll">
	<div class="wrapper hide-main-content"> 
		<section  class="page page-category contacts-us">
			   
			   <?php include 'includes/nav.php'; ?>
			   
				<!--Banner-->
				<section class="page-heading">
					<div class="title-slide">
						<div class="container">
							<div class="banner-content slide-container">									
								<div class="page-title">
									<h3>Contact us</h3>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!--End Banner-->
				<div class="page-content">					
					<!-- Breadcrumbs -->
					<div class="breadcrumbs">
						<div class="container">
							<div class="row">
								<div class="col-md-9">
									<ul>
										<li class="home"><a href="#"><i class="fa fa-home"></i> Home</a></li>
										<li><span>//</span></li>
										<li class="category-2"><a href="#">Contact us</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- End Breadcrumbs -->
					<!-- Our Team -->
					<section id="our-team" class="our-team-page">				
						<div class="our-team-head">
							<div class="container">
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-12">
										<div class="headding-title">
											<h4>We'd love to hear from you</h4>
											<div class="headding-bottom"></div>
										</div>
										<div class="headding-content">
											<p>Have questions about how Athlete  can help you streamline operations, improve performance and easily monitor the success of each department? Wondering 
											how the integrated dealer management system works, and how it can work for you?
											</p>
											<p>
												Give us a call at <span>1-234-567-890</span> or send us a message using the form below and we'll connect you with a dealer management software expert who can answer 
												your questions.
											</p>
										</div>
									</div>
									<div class="col-md-6 col-sm-12 col-xs-12">
										<div class="contact">
											<h4>Contact Form</h4>
											<div class="headding-bottom"></div>
											<form id="main-contact-form" class="main-contact-form row" name="contact-form" method="post">
												<div class="form-group col-md-12">
													<input type="text" name="name" class="control" required="required" placeholder="Your Name">
												</div>
												<div class="form-group col-md-12">
													<input type="email" name="email" class="control" required="required" placeholder="Your Email">
												</div>									
												<div class="form-group col-md-12">
													<textarea name="message" id="message" required="required" class="control" rows="8" placeholder="Your Message"></textarea>
												</div>                        
												<div class="form-group form-submit col-md-12">
													<button type="submit" name="submit" class="btn-submit">Send Message</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</section>
		
				<?php include 'includes/footer.php'; ?>


		<!--To Top-->
		<div id="copyright">
			<div class="container">

				<div class="back-to-top"><a title="BACK_TO_TOP" href="#top"><i class="fa fa-chevron-up"></i></a></div>

				<div class="clrDiv"></div>
			</div>
		</div>
		<!--End To Top-->
	</div>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
	<script type="text/javascript" src="js/jquery-2.1.0.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/shortcode-frontend.js"></script>
	<script type="text/javascript" src="js/jquery.mixitup.js"></script>
	<script type="text/javascript" src="js/classie.js"></script>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="js/waypoints.js"></script>	
	<script type="text/javascript" src="js/home.map.js"></script>	
	<script type="text/javascript" src="js/theme.js"></script>
	<script type="text/javascript" src="js/template.js"></script>
	<script type="text/javascript" src="js/dropdown.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
          
</body>
</html>
