<?php 
	
	{
	include 'functions.php';
	
	$products = get_products();
    
        if(isset($_GET['product']))
    {
        $product = get_products(array('att_str_where' => " AND products.alias = '" . $_GET['product'] . "'"));
    
            $productname = $product[0]['name'];
            $productid = $product[0]['id'];
    
     
    if ($productid > 1) { 
        
        $productid = $productname;     
    }  
    
    }
    
    else {
        $productname = 'Shop';
    }
?>			

<!doctype html>
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en"><!--<![endif]-->
    <head>
        <title>Shop</title>
		<!--meta info-->
		<meta charset="utf-8">
		<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
		<meta name="author" content="">
		<meta name="keywords" content="">
		<meta name="description" content="">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.css" type="text/css">
        <!--[if !IE 9]><!-->
		<link rel="stylesheet" href="css/effect.css" type="text/css">
		<link rel="stylesheet" href="css/animation.css" type="text/css">
		<!--<![endif]-->
        <link rel="stylesheet" href="css/masterslider.css" type="text/css">
        <link rel="stylesheet" type="text/css" media="all" href="css/owl.carousel.css">
        <link rel="stylesheet" type="text/css" media="all" href="css/owl.transitions.css">
        <link rel="stylesheet" href="css/jquery.custombox.css" type="text/css">
		<link rel="stylesheet" href="css/color.css" type="text/css">
		
		
		

    </head> 
    <body id="page-top" class="" data-offset="90" data-target=".navigation" data-spy="scroll">
        <div class="wrapper hide-main-content"> 
            <section class="page page-product">
             	<?php include 'includes/nav.php'; ?>
             	
        <?php }
			
		// Get products 
	    // --------------------------------
        
	    	if(isset($_GET['product']))
			{
		        $product = get_products(array('att_str_where' => " AND products.alias = '" . $_GET['product'] . "'"));
		
		
		        $num_products = count($products); 
			
		?>
		
		
		<!--Banner-->
                    <section class="page-heading">
                        <div class="title-slide">
                            <div class="container">
                                <div class="banner-content">									
                                    <div class="page-title">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
					
					<?php include 'includes/social-icons.php'; ?>

                    <!--End Banner-->
                    <div class="page-content">					
                        <!-- Breadcrumbs -->
                        <div class="breadcrumbs">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-9">
                                        <ul>
                                            <li class="home"><a href="/"><i class="fa fa-home"></i> Home</a></li>
                                            <li><span>//</span></li>
                                            <li class="home"><a href="/shop">Shop</a></li>
                                            <li><span>//</span></li>
                                            <li class="category-2"><?php echo $product[0]['name']; ?></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Breadcrumbs -->
                        <div class="product-detail">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="product-view">
                                            <div class="product-essential col-md-12" style="background-color: white; color: black;">
                                                <div class="product-img-box col-md-4">
                                                    <a href="#"><img alt="" src="assets/images/products/<?php echo $product[0]['image']; ?>"></a>
<!--
                                                    <div class="more-views">
                                                        <div id="owl-demo" class="owl-carousel owl-theme">
                                                            <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                            <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                            <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                            <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                            <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                            <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                            <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                            <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                            <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                            <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                        </div>
                                                        <!--  <a class="jcarousel-control-prev" href="#"><i class="fa fa-caret-left"></i></a>
                                                         <a class="jcarousel-control-next" href="#"><i class="fa fa-caret-right"></i></a> -->
<!--
                                                        <div class="customNavigation">
                                                            <a class="btn prev"><i class="fa fa-caret-left"></i></a>
                                                            <a class="btn next"><i class="fa fa-caret-right"></i></a>
                                                        </div>
                                                    </div>
-->
                                                </div>
                                                <div class="product-shop col-md-8">
                                                    <div class="product-name">
                                                        <h1><?php echo $product[0]['name']; ?></h1>
                                                    </div>
                                                    <div class="meta-box">
                                                        <div class="price-box">																										
                                                            <span class="special-price"><?php echo $product[0]['price']; ?></span>
                                                        </div>                                                    
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="add-to-box">
                                                                                                                                                                          
													<?php echo $product[0]['shopify']; ?>
                                                 
                                                    </div>
                                                    <div class="cat-list">
                                                        <label>Categories</label><span>:</span>
                                                        <ul>
                                                            <li><?php echo $product[0]['category']; ?></li>
                                                        </ul>
                                                    </div>
                                                    <div class="tags-list">
                                                        <label>Tags</label><span>:</span>
                                                        <ul>
                                                            <li><?php echo $product[0]['tags']; ?></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="tabs" class="product-collateral">
                                                <ul>
                                                    <li><a href="#tabs-1">Description</a></li>
<!--                                                     <li><a href="#tabs-2">Reviews</a></li> -->
                                                </ul>
                                                <div id="tabs-1" class="box-collateral">
                                                    <?php echo $product[0]['description']; ?>
                                                </div>
                                            </div>                                      
                                    	</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>
             	
             	<?php }
    
    
        
    // All products 
    // --------------------------------
    else
    {
	
	?>				
					<!--Banner-->
					<section class="page-heading">
						<div class="title-slide">
							<div class="container">
								<div class="banner-content">									
									<div class="page-title">
										<h3>Shop</h3>
									</div>
								</div>
							</div>
						</div>
					</section>
					<!--End Banner-->
					<div class="page-content">					
						<!-- Breadcrumbs -->
						<div class="breadcrumbs">
							<div class="container">
								<div class="row">
									<div class="col-md-9">
										<ul>
											<li class="home"><a href="/"><i class="fa fa-home"></i> Home</a></li>
											<li><span>//</span></li>
											<li class="category-2"><a href="#">Shop</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<!-- End Breadcrumbs -->
						<div class="product-list">
							<div class="container">
								<div class="row">
									<div class="col-md-9">
										<div class="toolbar">
											<div class="sorter">
												<div class="sort-by">
													<select onchange="setLocation(this.value)">
														<option selected="selected" value="">Sort By</option>
														<option value="">Position</option>
														<option value="">Name</option>
														<option value="">Price</option>
													</select>
												</div>                                                
											</div>
										</div>
										<div class="row">
											
											<?php
						            
									            foreach($products as $i => $v)
														{
													
											?>

											
											<div class="col-md-4 col-sm-6 col-xs-12">
												<div class="product-image-wrapper">
													<div class="product-content">
														<div class="product-image shop-products">
															<a href="?product=<?php echo $v['alias']; ?>"><img class="product-img" alt="" src="assets/images/products/<?php echo $v['image']; ?>"></a>
														</div>
														<div class="info-products">
															<div class="product-list-name">
																<a href="product-detail.html"><?php echo $v['name']; ?></a>
																<div class="product-bottom"></div>
															</div>
															<div class="price-box">																										
																<span class="special-price"><?php echo $v['price']; ?></span>
															</div>
															<div class="actions">
																<ul>																	
																	<li><a href="?product=<?php echo $v['alias']; ?>"><i class="fa fa-shopping-cart"></i></a></li>
																	<li><a href="#"><i class="fa fa-heart"></i></a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
											</div>
									

											<?php
												 
												}
											?>
											

									</div>
										<div class="clearfix"></div>
										
									</div>
									<div class="col-md-3">
					
										<div class="product-categories">
											<h3 class="title">Product categories</h3>
											<ul>
												<?php 
						
											foreach($veneers as $i => $v)
											{
												$cur = (isset($_SESSION['by_category']) && $_SESSION['by_category'] == $v['alias']) ? ' selected' : '';
												
												?>
												<option value="?by_veneer=<?php echo $v['alias']; ?>"<?php echo $cur; ?>><?php echo $v['title']; ?> (<?php echo '<span style="color: red;">' . $v['projects'] . '</span>'; ?>)</option>
												<li><a href="#" data-filter=".me">Mens</a></li>
											<?php } ?>
												
												<li><a href="/product-category/womens-fire-apparel">Womens</a></li>
												<li><a href="/product-category/firefighter-hats">Hats</a></li>
												<li><a href="/product-category/fitness-clothing">Fitness Apparel</a></li>
												<li><a href="/product-category/on-point-supplements">On Point Supplements</a></li>
												<li><a href="/product-category/kids">Kids</a></li>
												<li><a href="/product-category/gear">Gear</a></li>
												<li><a href="/product-category/accessories">Accessories</a></li>
											</ul>
										</div>
										<div class="product-popular">
											<h3 class="title">Popular products</h3>
											<ul>
												<li>
													<div class="product-image">
														<a href="#"><img alt="" src="images/popular-product.jpg"></a>
													</div>
													<div class="info-products">
														<div class="product-name">Lorem ipsum dolor sit amet consectetur</div>
														<div class="price-box">																										
															<span class="regular-price">$200.60</span>
														</div>
													</div>
												</li>												
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
                </div>
            </section>
            <?php include 'includes/social-icons.php'; ?>
            
            
        <?php } ?>
		
		
		<?php include 'includes/footer.php'; ?>
		
            <!--To Top-->
            <div id="copyright">
                <div class="container">

                    <div class="back-to-top"><a title="BACK_TO_TOP" href="#top"><i class="fa fa-chevron-up"></i></a></div>

                    <div class="clrDiv"></div>
                </div>
            </div>
            <!--End To Top-->
        </div>

        <script type="text/javascript" src="js/jquery-2.1.0.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui.js"></script>
        <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="js/waypoints.js"></script>
        <script type="text/javascript" src="js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="js/template.js"></script>
        <script type="text/javascript" src="js/masterslider.min.js"></script>	
        <script type="text/javascript" src="js/dropdown.js"></script>
        <script type="text/javascript" src="js/classie.js"></script>
        <script type="text/javascript" src="js/jquery.custombox.js"></script>
        <script type="text/javascript" src="js/main.js"></script>
        <script type="text/javascript">
            $(function() {
                $('.quickview').on('click', function(e) {
                    $.fn.custombox(this);
                    var owl = $("#owl-demo");
                    owl.owlCarousel({
                        items: 5,
                        itemsDesktop: [1000, 5],
                        itemsDesktopSmall: [900, 3],
                        itemsTablet: [600, 2],
                        itemsMobile: false,
						pagination: false
                    });
                    $(".next").click(function() {
                        owl.trigger('owl.next');
                    })
                    $(".prev").click(function() {
                        owl.trigger('owl.prev');
                    })
                    e.preventDefault();
                });
            });

            $('#price-filter').slider({
                range: true,
                min: 0,
                max: 999,
                values: [100, 700],
                slide: function(event, ui)
                {
                    $('#price-filter-value-1').text(ui.values[0]);
                    $('#price-filter-value-2').text(ui.values[1]);
                    var min = ui.values[0] / 999 * 90;
                    var max = ui.values[1] / 999 * 90;
                    $('.min-filter').css('left', min + '%');
                    $('.max-filter').css('left', max + '%');
                }
            });
            function increaseQty(id){
                var qtya = $('.qty-'+id).val();
                var qtyb = qtya * 1 + 1;
                $('.qty-'+id).val(qtyb);
            }
            function decreaseQty(id){
                var qtya = $('.qty-'+id).val();
                var qtyb = qtya * 1 - 1;
                if (qtyb < 1) {
                    qtyb = 1;
                }
                $('.qty-'+id).val(qtyb);
            }
        </script>
    </body>
</html>
