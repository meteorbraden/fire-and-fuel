<!doctype html>
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en"><!--<![endif]-->
<head>
<title>Fire and Fuel Apparel</title>
<!--meta info-->
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
<meta name="author" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<link rel="stylesheet" href="css/style.css" type="text/css">
<link rel="stylesheet" href="css/bootstrap.css" type="text/css">
<link rel="stylesheet" href="css/font-awesome.css" type="text/css">
<!--[if !IE 9]><!-->
<link rel="stylesheet" href="css/effect.css" type="text/css">
<link rel="stylesheet" href="css/animation.css" type="text/css">
<!--<![endif]-->
<link rel="stylesheet" href="css/masterslider.css" type="text/css">
<link rel="stylesheet" href="css/ms-fullscreen.css" type="text/css">
<link rel="stylesheet" type="text/css" media="all" href="css/owl.carousel.css">
<link rel="stylesheet" type="text/css" media="all" href="css/owl.transitions.css">
<link rel="stylesheet" href="css/color.css" type="text/css">
<link rel="stylesheet" href="css/colorbox.css" />
<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>
</head> 
<body id="page-top" class="" data-offset="90" data-target=".navigation" data-spy="scroll">
<div id="fb-root"></div>
	<div class="wrapper hide-main-content"> 
		<section  class="page shop one-page">
			
			<?php include 'includes/nav.php'; ?>
			
				<div class="page-content">														
					<!-- Calendar -->
					<section class="calendar">
						<div class="calendar-banner page-heading slide-container">							
							<div class="container">
								<div class="row">										
									<div class="calendar-title">
										<div class="next-month">
											<div class="prev button-nav-prev">
												<button class="circle icon-wrap t-nav-prev">
													<i class="fa fa-angle-left"></i>
												</button>
											</div>
											<div class="owl-carousel" data-nav="t-nav-" data-plugin-options='{"autoPlay":false,"autoHeight":true, "mouseDrag": false'>
												<div class="calendar-month">September,&nbsp;2016</div>
											</div>
											<div class="next next-nav-next">
												<button class="circle icon-wrap t-nav-next">
													<i class="fa fa-angle-right"></i>
												</button>
											</div>
										</div>
									</div>									
								</div>
							</div>							
						</div>
					
						<div class="list-event">
							<div class="owl-carousel" data-nav="t-nav-" data-plugin-options='{"autoPlay":false,"autoHeight":true, "mouseDrag": false'>																								
								<div class="calendar-full-view">	
									<div class="calendar-main">
										<div class="container">
											<div class="row">											
												<!-- Calendar Title -->
												<div class="calendar-full">
													<div class="calendar-day">
														<div class="mon">MON</div>
														<div class="tue">TUE</div>
														<div class="wed">WED</div>
														<div class="thu">THU</div>
														<div class="fri">FRI</div>
														<div class="sat">SAT</div>
														<div class="sun">SUN</div>
													</div>
													<div class="calendar-date">
														<ul class="calendar-date-1">
															<li>
																<div class="calendar-day calendar-last">27</div>
															</li>
															<li>
																<div class="calendar-day calendar-last">28</div>
															</li>
															<li>
																<div class="calendar-day calendar-last">29</div>
															</li>
															<li>
																<div class="calendar-day calendar-last">30</div>
															</li>
															<li>
																<div class="calendar-day calendar-last">31</div>
															</li>
															<li>
																<div class="calendar-day">1</div>
															</li>
															<li>
																<div class="calendar-day">2</div>
															</li>												
														</ul>
														<ul class="calendar-date-2">													
															<li>
																<div class="calendar-day">3</div>
															</li>
															<li>
																<div class="calendar-day">4</div>
															</li>
															<li>
																<div class="calendar-day">5</div>											
															</li>
															<li>
																<div class="calendar-day">6</div>
															</li>
															<li>
																<div class="calendar-day">7</div>
															</li>
															<li>
																<div class="calendar-day">8</div>
															</li>
															<li>
																<div class="calendar-day">9</div>
															</li>
														</ul>
														<ul class="calendar-date-3">
															<li>
																<div class="calendar-day">10</div>
															</li>
															<li>
																<div class="calendar-day">11</div>
															</li>
															<li>
																<div class="calendar-day">12</div>
															</li>
															<li>
																<div class="calendar-day">13</div>															
															</li>
															<li>
																<div class="calendar-day">19</div>
															</li>
															<li>
																<div class="calendar-day">20</div>
															</li>
															<li>
																<div class="calendar-day">21</div>
															</li>
															<li>
																<div class="calendar-day">22</div>
															</li>
															<li>
																<div class="calendar-day">23</div>
															</li>
														</ul>
														<ul class="calendar-date-5">
															<li>
																<div class="calendar-day">24</div>
															</li>
															<li>
																<div class="calendar-day">25</div>
															</li>
															<li>
																<div class="calendar-day">26</div>
															</li>
															<li>
																<div class="calendar-day">27</div>
															</li>
															<li>
																<div class="calendar-day">28</div>
															</li>
															<li>
																<div class="calendar-day">29</div>
															</li>
															<li>
																<div class="calendar-day">30</div>
															</li>
														</ul>
													</div>
												</div>									
											</div>
										</div>
									</div>
								</div>
								<div class="calendar-full-view">										
									<div class="calendar-main">
										<div class="container">
											<div class="row">											
												<!-- Calendar Title -->
												<div class="calendar-full">
													<div class="calendar-day">
														<div class="mon">MON</div>
														<div class="tue">TUE</div>
														<div class="wed">WED</div>
														<div class="thu">THU</div>
														<div class="fri">FRI</div>
														<div class="sat">SAT</div>
														<div class="sun">SUN</div>
													</div>
													<div class="calendar-date">
														<ul class="calendar-date-1">
															<li>
																<div class="calendar-day">1</div>
															</li>
															<li>
																<div class="calendar-day">2</div>
															</li>
															<li  class="calendar-note">
																<div class="calendar-day">3</div>
															</li>
															<li>
																<div class="calendar-day">4</div>
															</li>
															<li>
																<div class="calendar-day">5</div>
															</li>
															<li>
																<div class="calendar-day">6</div>
															</li>
															<li>
																<div class="calendar-day">7</div>
															</li>
														</ul>
														<ul class="calendar-date-2">
															<li>
																<div class="calendar-day">8</div>
															</li>
															<li>
																<div class="calendar-day">9</div>
															</li>
															<li>
																<div class="calendar-day">10</div>
															</li>
															<li>
																<div class="calendar-day">11</div>
															</li>
															<li>
																<div class="calendar-day">12</div>
															</li>
															<li class="calendar-note">
																<div class="calendar-day">13</div>
															</li>
															<li>
																<div class="calendar-day">14</div>
															</li>
														</ul>
														<ul class="calendar-date-3">													
															<li>
																<div class="calendar-day">15</div>
															</li>
															<li>
																<div class="calendar-day">16</div>
															</li>
															<li>
																<div class="calendar-day">17</div>
															</li>
															<li class="calendar-note">
																<div class="calendar-day">18</div>
															</li>
															<li>
																<div class="calendar-day">19</div>
															</li>
															<li>
																<div class="calendar-day">20</div>
															</li>
															<li>
																<div class="calendar-day">21</div>
															</li>
														</ul>
														<ul class="calendar-date-4">												
															<li>
																<div class="calendar-day">22</div>
															</li>
															<li>
																<div class="calendar-day">23</div>
															</li>
															<li>
																<div class="calendar-day">24</div>
															</li>
															<li>
																<div class="calendar-day">25</div>
															</li>
															<li>
																<div class="calendar-day">26</div>
															</li>
															<li>
																<div class="calendar-day">27</div>
															</li>
															<li>
																<div class="calendar-day">28</div>
															</li>
														</ul>
														<ul class="calendar-date-5">													
															<li>
																<div class="calendar-day">29</div>
															</li>
															<li>
																<div class="calendar-day">30</div>
															</li>
															<li>
																<div class="calendar-day calendar-last">1</div>
															</li>
															<li>
																<div class="calendar-day calendar-last">2</div>
															</li>
															<li>
																<div class="calendar-day calendar-last">3</div>
															</li>
															<li>
																<div class="calendar-day calendar-last">4</div>
															</li>
															<li>
																<div class="calendar-day calendar-last">5</div>
															</li>
														</ul>
													</div>
												</div>									
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					<!-- End Calendar -->
					
				<br />
				<br />
		
				<?php include 'includes/footer.php'; ?>
		
		<!--To Top-->
		<div id="copyright">
			<div class="container">

				<div class="back-to-top"><a title="BACK_TO_TOP" href="#top"><i class="fa fa-chevron-up"></i></a></div>

				<div class="clrDiv"></div>
			</div>
		</div>
		<!--End To Top-->
	</div>
	<script type="text/javascript" src="js/jquery-2.1.0.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/shortcode-frontend.js"></script>
	<script type="text/javascript" src="js/jquery.mixitup.js"></script>
	<script type="text/javascript" src="js/classie.js"></script>
	<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="js/waypoints.js"></script>	
	<script type="text/javascript" src="js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="js/theme.js"></script>
	<script type="text/javascript" src="js/template.js"></script>
	<script type="text/javascript" src="js/dropdown.js"></script>
	
	<script type="text/javascript" src="js/main.js"></script>         
</body>
</html>
