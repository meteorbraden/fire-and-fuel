<?php

	// Connect local
	// -------------
	/* */
	$db = mysqli_connect(
						'localhost', 		// HOST
						'root',				// USERNAME
						'root',				// PASSWORD
						'fireandfuel'	// DATABASE
						)
						or die('Error ' . mysqli_error($db));
			
	

/* //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// */

	// Get Blog products
	// ------------
	function get_products($params = array())
		
		{
			global $db;
			
			// Params
			$att_str_from = isset($params['att_str_from']) ? $params['att_str_from'] : '';
			$att_str_where = isset($params['att_str_where']) ? $params['att_str_where'] : '';
			$att_str = isset($params['att_str']) ? $params['att_str'] : '';
	
			
			$query = 'SELECT 
							products.*
							
						FROM products AS products ' . $att_str_from . '
						
						WHERE 1' . $att_str_where . '
							ORDER BY products.name ASC' . $att_str;
						
			$result = $db->query($query) or die('Error in the consult..' . mysqli_error($db));
			
			$return = array();
			while($row = $result->fetch_assoc())
				$return[] = $row;
			
				
			return $return;
		}
		
		
	?>
	
