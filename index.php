<!doctype html>
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en"><!--<![endif]-->
<head>
<title>Fire and Fuel Apparel</title>
<!--meta info-->
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
<meta name="author" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<link rel="stylesheet" href="css/style.css" type="text/css">
<link rel="stylesheet" href="css/bootstrap.css" type="text/css">
<link rel="stylesheet" href="css/font-awesome.css" type="text/css">
<!--[if !IE 9]><!-->
<link rel="stylesheet" href="css/effect.css" type="text/css">
<link rel="stylesheet" href="css/animation.css" type="text/css">
<!--<![endif]-->
<link rel="stylesheet" href="css/masterslider.css" type="text/css">
<link rel="stylesheet" href="css/ms-fullscreen.css" type="text/css">
<link rel="stylesheet" type="text/css" media="all" href="css/owl.carousel.css">
<link rel="stylesheet" type="text/css" media="all" href="css/owl.transitions.css">
<link rel="stylesheet" href="css/color.css" type="text/css">
<link rel="stylesheet" href="css/colorbox.css" />
</head> 
<body id="page-top" class="" data-offset="90" data-target=".navigation" data-spy="scroll">
<div id="fb-root"></div>
<script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.colorbox.js"></script>
<!--
    <script>
	     (function($) {
		      function openColorBox(){
		        $.colorbox({href:"popup.html"});
		      }
		      
		      setTimeout(openColorBox, 3000);
		      })(jQuery);
    </script>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
-->
	<div class="wrapper hide-main-content"> 
		<section  class="page shop one-page">
			
			<?php include 'includes/nav.php'; ?>
			
				<!--Banner-->
				<section class="slide-container to-top">
					<div style="height: 900px;" class="ms-fullscreen-template" id="slider1-wrapper">
						<!-- masterslider -->
						<div class="master-slider ms-skin-default" id="masterslider-index">
							<div class="ms-slide slide-1" data-delay="0">
								<img src="images/blank.gif" data-src="assets/images/bg/new-hero.jpg" alt="lorem ipsum dolor sit"/>																											
								<h3 class="ms-layer hps-title3" style="left:95px; margin-left: 258px !important;"
									data-type="text"
									data-delay="1900"
									data-duration="0"
									data-effect="rotate3dtop(-100,0,0,40,t)"
									data-ease="easeOutExpo"
								>
									A wide array of 
								</h3>
								
								<h3 class="ms-layer hps-title4" style="left:201px; margin-left: 258px !important;"
									data-type="text"
									data-delay="2500"
									data-duration="0"
									data-effect="rotate3dtop(-100,0,0,18,t)"
									data-ease="easeOutExpo"
								>
									Firefighter. EMS. and Police Apparel
								</h3>
							</div>
<!--
							<div class="ms-slide slide-2" data-delay="0">
							   <div class="slide-pattern"></div>							  
								<img src="images/blank.gif" data-src="assets/images/firetruck-2.jpg" alt="lorem ipsum dolor sit"/>
								<h3 class="ms-layer hps-title1" style="left:101px"
									data-type="text"
									data-ease="easeOutExpo"
									data-delay="1000"
									data-duration="0"
									data-effect="skewleft(30,80)">
									Athlete Fitness Club
								</h3>																												
								<h3 class="ms-layer hps-title3" style="left:95px"
									data-type="text"
									data-delay="1900"
									data-duration="0"
									data-effect="rotate3dtop(-100,0,0,40,t)"
									data-ease="easeOutExpo"
								>
									Make You Be The Fighter
								</h3>
								
								<h3 class="ms-layer hps-title4" style="left:101px"
									data-type="text"
									data-delay="2500"
									data-duration="0"
									data-effect="rotate3dtop(-100,0,0,18,t)"
									data-ease="easeOutExpo"
								>
									Try A Free Class
								</h3>
							</div>	
							
							<div class="ms-slide slide-3" data-delay="0">
							   <div class="slide-pattern"></div>							  
								<img src="images/blank.gif" data-src="assets/images/firetruck-3.jpg" alt="lorem ipsum dolor sit"/>
								<h3 class="ms-layer hps-title1" style="left:101px"
									data-type="text"
									data-ease="easeOutExpo"
									data-delay="1000"
									data-duration="0"
									data-effect="skewleft(30,80)"
								>
									Athlete Fitness Club
								</h3>																												
								<h3 class="ms-layer hps-title3" style="left:95px"
									data-type="text"
									data-delay="1900"
									data-duration="0"
									data-effect="rotate3dtop(-100,0,0,40,t)"
									data-ease="easeOutExpo"
								>
									Make You Be The Fighter
								</h3>
								
								<h3 class="ms-layer hps-title4" style="left:101px"
									data-type="text"
									data-delay="2500"
									data-duration="0"
									data-effect="rotate3dtop(-100,0,0,18,t)"
									data-ease="easeOutExpo"
								>
									Try A Free Class
								</h3>
							</div>
-->
							<!--div class="ms-slide slide-4">
							   <div class="slide-pattern"></div>
								<video data-autopause="false" data-mute="true" data-loop="true" data-fill-mode="fill">
									<source src="images/video/demo.mp4" type="video/mp4"/>									
								</video>  
							</div-->
						</div>
						<!-- end of masterslider -->
						<div class="to-bottom" id="to-bottom"><i class="fa fa-angle-down"></i></div>
					</div>
				</section>
				<div class="cart-wishlist">
					<div class="cart-store">
						<div class="icon-cart">
							<div class="my-cart">
								<button class="icon-fa"><i class="fa fa-facebook"></i></button>
							</div>
							</div>
						</div>
					</div>
					<div class="wishlist-store">
						<div class="icon-wishlist">
							<div class="my-wishlist">
								<button class="icon-fa"><i class="fa fa-twitter"></i></button>
							</div>														
							</div>
						</div>
					</div>
					<div class="cart-store" style="top: 200px;">
						<div class="icon-cart">
							<div class="my-cart">
								<button class="icon-fa"><i class="fa fa-google-plus"></i></button>
							</div>
							</div>
						</div>
					</div>
					<div class="wishlist-store" style="top: 250px;">
						<div class="icon-wishlist">
							<div class="my-wishlist">
								<button class="icon-fa"><i class="fa fa-pinterest"></i></button>
							</div>														
							</div>
						</div>
					</div>								
				</div>
				
				<!--End Banner-->							
				<div class="contents-main" id="contents-main">						
					<!--Price Table-->
<!--
					<section class="price-table">
						<div class="container">
							<div class="row">							
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="col-md-4 col-sm-12 col-xs-12">
										<div class="col-md-12 price-table-content">
											<div class="price-table-img">
												<img alt="" src="images/newproducts/images-3.png">
											</div>
											<div class="price-table-text">
												<h3>Mens</h3>
												<h2>Clothing</h2>
												<div class="border-bottom"></div>
												<p>Phasellus molestie magna non est bibendum non venenatis nisl tempor lorem ipsum dolor.</p>
												<a href="#"><span>Shop Now</span></a>
											</div>
										</div>							
									</div>
									<div class="col-md-4 col-sm-12 col-xs-12">
										<div class="col-md-12 price-table-content">
											<div class="price-table-img">
												<img alt="" src="images/newproducts/images-2.png">
											</div>
											<div class="price-table-text">
												<h3>Womens</h3>
												<h2>Clothing</h2>
												<div class="border-bottom"></div>
												<p>Phasellus molestie magna non est bibendum non venenatis nisl tempor lorem ipsum dolor.</p>
												<a href="#"><span>Shop Now</span></a>
											</div>
										</div>							
									</div>
									<div class="col-md-4 col-sm-12 col-xs-12">
										<div class="col-md-12 price-table-content">
											<div class="price-table-img">
												<img alt="" src="images/newproducts/images-1.png">
											</div>
											<div class="price-table-text">
												<h3>Fitness</h3>
												<h2>Products</h2>
												<div class="border-bottom"></div>
												<p>Phasellus molestie magna non est bibendum non venenatis nisl tempor lorem ipsum dolor.</p>
												<a href="#"><span>Shop Now</span></a>
											</div>
										</div>								
									</div>
								</div>
							</div>
						</div>
					</section>
-->
					<section class="price-table">
						<div class="container">
							<div class="row">							
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="col-md-4 col-sm-12 col-xs-12">
										<div class="col-md-12" style="padding-right: 0px;">
											<img class="featured-item-img" alt="" src="assets/images/womens-shirt-1.jpg">
											<div class="featured-category">
												Womens		
											</div>
										</div>							
									</div>
									<div class="col-md-4 col-sm-12 col-xs-12">
										<div class="col-md-12">
											<img class="featured-item-img" alt="" src="assets/images/mens-shirt-7.jpg">
											<div class="featured-category">
												Mens	
											</div>
										</div>							
									</div>
									<div class="col-md-4 col-sm-12 col-xs-12">
										<div class="col-md-12">
											<img class="featured-item-img" alt="" src="assets/images/on-point-supplements.jpg">
											<div class="featured-category">
												Fitness		
											</div>
										</div>								
									</div>
								</div>
							</div>
						</div>
					</section>
					<!--End Price Table-->
					
					<!--New Products-->
						<section class="new-product scroll-to">
							<div class="container">
								<div class="row">
									<div class="title-name">
										<h4>Featured Products</h4>
									</div>
									<div class="col-md-3 col-sm-6 col-xs-12">
										<div class="product-image-wrapper">
											<div class="product-content">
												<div class="product-image">
													<a href="#"><img alt="" src="assets/images/mens-shirt-1.jpg"></a>
												</div>
												<div class="info-products">
													<div class="product-name" >
														<a href="#">Heroes Without Capes</a>
														<div class="product-bottom"></div>
													</div>
													<div class="price-box">																										
														<span class="special-price">$19.99-$23.99</span>
													</div>
													<div class="actions">
														<ul>
															<li><a href="#"><i class="fa fa-info"></i></a></li>
															<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
															<li><a href="#"><i class="fa fa-heart"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-3 col-sm-6 col-xs-12">
										<div class="product-image-wrapper">
											<div class="product-content">
												<div class="product-image">
													<a href="#"><img alt="" src="assets/images/womens-shirt-1.jpg"></a>
												</div>
												<div class="info-products">
													<div class="product-name" >
														<a href="#">Limitless Tank Top</a>
														<div class="product-bottom"></div>
													</div>
													<div class="price-box">																										
														<span class="special-price">$19.99</span>
													</div>
													<div class="actions">
														<ul>
															<li><a href="#"><i class="fa fa-info"></i></a></li>
															<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
															<li><a href="#"><i class="fa fa-heart"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-3 col-sm-6 col-xs-12">
										<div class="product-image-wrapper">
											<div class="product-content">
												<div class="product-image">
													<a href="#"><img alt="" src="assets/images/on-point-supplements.jpg"></a>
												</div>
												<div class="info-products">
													<div class="product-name" >
														<a href="#">Deploy Pre-Workout</a>
														<div class="product-bottom"></div>
													</div>
													<div class="price-box">																										
														<span class="special-price">$38.99</span>
													</div>
													<div class="actions">
														<ul>
															<li><a href="#"><i class="fa fa-info"></i></a></li>
															<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
															<li><a href="#"><i class="fa fa-heart"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-3 col-sm-6 col-xs-12">
										<div class="product-image-wrapper">
											<div class="product-content">
												<div class="product-image">
													<a href="#"><img alt="" src="assets/images/products/mens/dead-head-t-shirt.jpg"></a>
												</div>
												<div class="info-products">
													<div class="product-name" >
														<a href="#">Dead Head T-Shirt</a>
														<div class="product-bottom"></div>
													</div>
													<div class="price-box">																										
														<span class="special-price">$19.99-$25.99</span>
													</div>
													<div class="actions">
														<ul>
															<li><a href="#"><i class="fa fa-info"></i></a></li>
															<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
															<li><a href="#"><i class="fa fa-heart"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-3 col-sm-6 col-xs-12">
										<div class="product-image-wrapper">
											<div class="product-content">
												<div class="product-image">
													<a href="#"><img alt="" src="assets/images/fire-and-fuel-hat.jpg"></a>
												</div>
												<div class="info-products">
													<div class="product-name" >
														<a href="#">Fire and Fuel Hat</a>
														<div class="product-bottom"></div>
													</div>
													<div class="price-box">																										
														<span class="special-price">$18.99</span>
													</div>
													<div class="actions">
														<ul>
															<li><a href="#"><i class="fa fa-info"></i></a></li>
															<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
															<li><a href="#"><i class="fa fa-heart"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-3 col-sm-6 col-xs-12">
										<div class="product-image-wrapper">
											<div class="product-content">
												<div class="product-image">
													<a href="#"><img alt="" src="assets/images/mens-shirt-6.jpg"></a>
												</div>
												<div class="info-products">
													<div class="product-name" >
														<a href="#">Firefighter Eagle T-Shirt</a>
														<div class="product-bottom"></div>
													</div>
													<div class="price-box">																										
														<span class="special-price">$24.99-$28.99</span>
													</div>
													<div class="actions">
														<ul>
															<li><a href="#"><i class="fa fa-info"></i></a></li>
															<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
															<li><a href="#"><i class="fa fa-heart"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-3 col-sm-6 col-xs-12">
										<div class="product-image-wrapper">
											<div class="product-content">
												<div class="product-image">
													<a href="#"><img alt="" src="assets/images/thin-red-line-shirt.jpg"></a>
												</div>
												<div class="info-products">
													<div class="product-name" >
														<a href="#">Thin Red Line T-Shirt</a>
														<div class="product-bottom"></div>
													</div>
													<div class="price-box">																										
														<span class="special-price">$24.99-$26.99</span>
													</div>
													<div class="actions">
														<ul>
															<li><a href="#"><i class="fa fa-info"></i></a></li>
															<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
															<li><a href="#"><i class="fa fa-heart"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-3 col-sm-6 col-xs-12">
										<div class="product-image-wrapper">
											<div class="product-content">
												<div class="product-image">
													<a href="#"><img alt="" src="assets/images/womens-shirt-2.jpg"></a>
												</div>
												<div class="info-products">
													<div class="product-name" >
														<a href="#">Firefighter Challenge Tank</a>
														<div class="product-bottom"></div>
													</div>
													<div class="price-box">																										
														<span class="special-price">$19.99</span>
													</div>
													<div class="actions">
														<ul>
															<li><a href="#"><i class="fa fa-info"></i></a></li>
															<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
															<li><a href="#"><i class="fa fa-heart"></i></a></li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>								
								</div>						
							</div>
						</section>
					<!--End New Products-->
					
					<!--Discover-->
					<section class="training modern">
						<div class="wpb-wrapper">
							<!-- PARALLAX WINDOWS -->
							<div class="parallax-block-wrap-module auto-width">
								<div class="parallax-block bt-advance-parallax" id="parallax-block-modern">
									<!-- PARALLAX CONTENT -->
									<div class="control-button">
										<div class="nav-wrap hidden">
											<div class="nav-wrap-in next">
												<span class="nav-next"></span>
											</div>
											<div class="nav-wrap-in prev">
												<span class="nav-prev"></span>
											</div>
										</div>
										<div class="button-wrap">
											<span class="button close-btn"></span>
										</div>
									</div>
									<div class="parallax-background">
										<!--Textured and color-->
										<div class="parallax-background-overlay"></div>
										<img alt="" src="assets/images/bg/firefighter-bw.jpg">
									</div>
									
									<div class="parallax-block-content default-pos">
										<div class="container">
											<div class="parallax-text parallax-shop parallax-gallery">																								
												<h1>About Us</h1> 
												<p>Fire And Fuel Apparel, the leading Firefighter Store presents a wide array of firefighter apparel, clothing and accessories for men and women, inspired by some of the bravest and most daring of today’s heroes. Fire and Fuel Apparel supplies firefighter fitness apparel, shirts, sweatshirts, hoodies, key chains, hats and much more with striking colors and designs that do justice to the great men and women saving lives across the country. With comfortable, durable materials and carefully designed, iconic insignia, Fire And Fuel Apparel creates clothing and accessories that are not only functional, but also stylish, unique and attractive. Each design is inspired by the fearlessness and daring of our everyday heroes, and what it truly means to be a firefighter, paramedic and EMS responder.</p>
											</div>
										</div>
									</div>
									
									<div class="overlay-loading"><span class="loading"><img src="images/loading-black.gif" alt=""/></span></div>    

									<!-- GALLERY CONTENT -->
									<div class="parallax-content-wrap parallax-shopping">
											<div class="parallax-content hidden">
												<div class="parallax-content-in">
													<div class="defxault">
														<div class="parallax-col">
															<div class="parallax-row in-pos box1">
																<div class="parallax-row-w1">
																	<div class="parallax-box-image">
																		<div class="thumb" onclick="showproduct()">
																			<img src="images/newproducts/product-1.jpg" alt=""/>
																		</div>																
																	</div>
																	<div class="parallax-box-image">
																		<div class="thumb" onclick="showproduct()">
																			<img src="images/newproducts/product-2.jpg" alt=""/>
																		</div>
																	</div>
																	<div class="parallax-box-image">
																		<div class="thumb w2" onclick="showproduct()">
																			<img src="images/newproducts/product-8.1.jpg" alt=""/>
																		</div>	
																	</div>	
																</div>
																<div class="parallax-row-w2">
																	<div class="parallax-box-image">
																		<div class="thumb w3 mr-top" onclick="showproduct()">
																			<img src="images/newproducts/product-9.1.jpg" alt=""/>
																		</div>
																	</div>
																</div>
																<div class="parallax-row-w3" onclick="showproduct()">
																	<div class="parallax-box-image">
																		<div class="thumb">
																			<img src="images/newproducts/product-3.1.jpg" alt=""/>
																		</div>
																	</div>
																	<div class="parallax-box-image">
																		<div class="thumb w2" onclick="showproduct()">
																			<img src="images/newproducts/product-8.1.jpg" alt=""/>
																		</div>
																	</div>
																</div>
															</div>
															<div class="parallax-row in-pos box2">
																<div class="parallax-row-w1">
																	<div class="parallax-box-image">
																		<div class="thumb w2" onclick="showproduct()">
																			<img src="images/newproducts/product-8.1.jpg" alt=""/>
																		</div>
																	</div>
																</div>	
																<div class="parallax-row-w2">
																	<div class="parallax-box-image">
																		<div class="thumb" onclick="showproduct()">
																			<img src="images/newproducts/product-3.1.jpg" alt=""/>
																		</div>
																	</div>
																	<div class="parallax-box-image">
																		<div class="thumb" onclick="showproduct()">
																			<img src="images/newproducts/product-3.1.jpg" alt=""/>
																		</div>
																	</div>
																</div>	
																<div class="parallax-row-w2" onclick="showproduct()">	
																	<div class="parallax-box-image">
																		<div class="thumb w3">
																			<img src="images/newproducts/product-7.1.jpg" alt=""/>
																		</div>
																	</div>
																</div>
															</div>									
														</div>
														<div class="parallax-col">
															<div class="parallax-row in-pos box1">
																<div class="parallax-row-w1">
																	<div class="parallax-box-image">
																		<div class="thumb" onclick="showproduct()">
																			<img src="images/newproducts/product-1.jpg" alt=""/>
																		</div>
																	</div>
																	<div class="parallax-box-image">
																		<div class="thumb" onclick="showproduct()">
																			<img src="images/newproducts/product-2.jpg" alt=""/>
																		</div>
																	</div>
																	<div class="parallax-box-image">
																		<div class="thumb w2" onclick="showproduct()">
																			<img src="images/newproducts/product-8.1.jpg" alt=""/>
																		</div>
																	</div>
																</div>
																<div class="parallax-row-w2">
																	<div class="parallax-box-image">
																		<div class="thumb w3 mr-top" onclick="showproduct()">
																			<img src="images/newproducts/product-9.1.jpg" alt=""/>
																		</div>
																	</div>
																</div>
																<div class="parallax-row-w3">
																	<div class="parallax-box-image">
																		<div class="thumb" onclick="showproduct()">
																			<img src="images/newproducts/product-3.1.jpg" alt=""/>
																		</div>
																	</div>
																	<div class="parallax-box-image">
																		<div class="thumb w2" onclick="showproduct()">
																			<img src="images/newproducts/product-8.1.jpg" alt=""/>
																		</div>
																	</div>
																</div>
															</div>
															<div class="parallax-row in-pos box2">
																<div class="parallax-row-w1">
																	<div class="parallax-box-image">
																		<div class="thumb w2" onclick="showproduct()">
																			<img src="images/newproducts/product-8.1.jpg" alt=""/>
																		</div>
																	</div>
																</div>	
																<div class="parallax-row-w2">
																	<div class="parallax-box-image">
																		<div class="thumb" onclick="showproduct()">
																			<img src="images/newproducts/product-3.1.jpg" alt=""/>
																		</div>
																	</div>
																	<div class="parallax-box-image">
																		<div class="thumb" onclick="showproduct()">
																			<img src="images/newproducts/product-3.1.jpg" alt=""/>
																		</div>
																	</div>
																</div>	
																<div class="parallax-row-w2">	
																	<div class="parallax-box-image">
																		<div class="thumb w3" onclick="showproduct()">
																			<img src="images/newproducts/product-7.1.jpg" alt=""/>
																		</div>
																	</div>
																</div>
															</div>									
														</div>									
														<div class="parallax-col">
															<div class="parallax-row in-pos box1">
																<div class="parallax-row-w1">
																	<div class="parallax-box-image">
																		<div class="thumb" onclick="showproduct()">
																			<img src="images/newproducts/product-1.jpg" alt=""/>
																		</div>
																	</div>
																	<div class="parallax-box-image">
																		<div class="thumb" onclick="showproduct()">
																			<img src="images/newproducts/product-2.jpg" alt=""/>
																		</div>
																	</div>
																	<div class="parallax-box-image">
																		<div class="thumb w2" onclick="showproduct()">
																			<img src="images/newproducts/product-8.1.jpg" alt=""/>
																		</div>	
																	</div>	
																</div>
																<div class="parallax-row-w2" onclick="showproduct()">
																	<div class="parallax-box-image">
																		<div class="thumb w3 mr-top">
																			<img src="images/newproducts/product-9.1.jpg" alt=""/>
																		</div>
																	</div>
																</div>
																<div class="parallax-row-w3" onclick="showproduct()">
																	<div class="parallax-box-image">
																		<div class="thumb">
																			<img src="images/newproducts/product-3.1.jpg" alt=""/>
																		</div>
																	</div>
																	<div class="parallax-box-image">
																		<div class="thumb w2" onclick="showproduct()">
																			<img src="images/newproducts/product-8.1.jpg" alt=""/>
																		</div>
																	</div>
																</div>
															</div>
															<div class="parallax-row in-pos box2" onclick="showproduct()">
																<div class="parallax-row-w1">
																	<div class="parallax-box-image">
																		<div class="thumb w2">
																			<img src="images/newproducts/product-8.1.jpg" alt=""/>
																		</div>
																	</div>
																</div>	
																<div class="parallax-row-w2" onclick="showproduct()">
																	<div class="parallax-box-image">
																		<div class="thumb">
																			<img src="images/newproducts/product-3.1.jpg" alt=""/>
																		</div>
																	</div>
																	<div class="parallax-box-image">
																		<div class="thumb" onclick="showproduct()">
																			<img src="images/newproducts/product-3.1.jpg" alt=""/>
																		</div>
																	</div>
																</div>	
																<div class="parallax-row-w2">	
																	<div class="parallax-box-image">
																		<div class="thumb w3" onclick="showproduct()">
																			<img src="images/newproducts/product-7.1.jpg" alt=""/>
																		</div>
																	</div>
																</div>
															</div>									
														</div>
													</div>
												</div>
												<div class="content-show-large hidden">
													<div class="item-contain"></div>
													<div class="loading"><img src="images/loading-black.gif" alt=""/></div>
												</div>
											</div>
										</div>
									<!-- end GALLERY CONTENT -->				  
								</div>
							</div>
						</div>													
					</section>
					<!--End Discover-->
					
					<!--News-->	
					<section class="new-product" >
						<div class="container">
							<div class="row">							
								<div class="col-md-6 col-sm-6 col-xs-12">
									<div class="news-cont">
										<div class="title-page">
											<h4>Wall of Fame</h4>									
										</div>
										<p class="news-text">Submit your images to <span style="color: #AD3847;">support@fireandfuelapparel.com</span> for your chance to be featured on the wall.
										<div class="news-wapper">
											<div class="col-md-4 col-sm-4 col-xs-12">
												<div class="img-news">
													<img src="assets/images/wall-of-fame/14203209_964720796989196_2864428142275272077_n.jpg" alt=""/>
												</div>
											</div>
											<div class="col-md-4 col-sm-4 col-xs-12">
												<div class="img-news">
													<img src="assets/images/wall-of-fame/12963699_862846993843244_2389298379085110409_n.jpg" alt=""/>
												</div>
											</div>
											<div class="col-md-4 col-sm-4 col-xs-12">
												<div class="img-news">
													<img src="assets/images/wall-of-fame/12938225_860833364044607_3891516280023617680_n.jpg" alt=""/>
												</div>
											</div>
										</div>
										<div class="news-wapper">
											<div class="col-md-4 col-sm-4 col-xs-12">
												<div class="img-news">
													<img src="assets/images/wall-of-fame/12039744_862247507236526_5093346323941765467_n.jpg" alt=""/>
												</div>
											</div>
											<div class="col-md-4 col-sm-4 col-xs-12">
												<div class="img-news">
													<img src="assets/images/wall-of-fame/12715206_833025666825377_3356712406066142429_n.jpg" alt=""/>
												</div>
											</div>
											<div class="col-md-4 col-sm-4 col-xs-12">
												<div class="img-news">
													<img src="assets/images/wall-of-fame/1918069_849967291797881_395730987589640883_n.jpg" alt=""/>
												</div>
											</div>
											
										</div>
									</div>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-12">
									<div class="news-cont">
										<div class="title-page">
											<h4>Roll Call</h4>								
										</div>
										<p class="news-text">Bookmark Roll Call and stay updated on the topics that matter most in today’s fire service: significant incidents, LODDs, training and tricks of the trade.
										<div class="news-content">
											<ul>
												<li class="latest-news-item">
													<div class="news-item-inner">
														<div class="news-date">
															<div class="news-day">26</div>
															<div class="news-month">FEB</div>
														</div>
														<div class="news-info">
															<br />
															<div class="news-title">
																<a href="#">PAR extends beyond the fireground</a>
															</div>
															<br />
														</div>
													</div>
												</li>
												<li class="latest-news-item">
													<div class="news-item-inner">
														<div class="news-date">
															<div class="news-day">11</div>
															<div class="news-month">DEC</div>
														</div>
														<div class="news-info">
															<div class="news-title">
																<a href="#">Firefighter injured in 2007 McClung warehouse fire returns to Knoxville Fire Department Home ROLL CALL Firefighter in</a>
															</div>
														</div>
													</div>
												</li>
												<li class="latest-news-item">
													<div class="news-item-inner">
														<div class="news-date">
															<div class="news-day">11</div>
															<div class="news-month">DEC</div>
														</div>
														<div class="news-info">
															<div class="news-title">
																<a href="#">New Haven Fire Department Chief Allyn Wright to leave post Jan. 4</a>
															</div>
															<br />
														</div>
													</div>
												</li>											
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					<!--News-->
					<section class="new-product" style="background: #AD3846;">
						<div class="container">
							<div class="row">							
								<div class="col-md-6 col-sm-6 col-xs-12">
									<div class="news-cont">
											<h1 style="color:black; margin-top: 255px; position: absolute;">Stay Connected</h1>									
	
										</div>
									</div>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-12" style="float: right; background: #f7f7f7; padding: 0;">
									<div class="news-cont">
										<div class="news-wapper">
											<div class="col-md-6 col-sm-6 col-xs-12">
												<div class="img-news">
													<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FFireAndFuelApparel%2Fposts%2F981087005352575%3A0&width=500" width="270" height="235" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
												</div>
											</div>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<div class="img-news" style="margin-top: -10px;">
													<blockquote  class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Team Fire and Fuel Apparel Post<br> 🚒💦🔥Firefighter Athlete🔥💦🚒 <br>A team united by the common goal of… <a href="https://t.co/YxbYzRfKWz">https://t.co/YxbYzRfKWz</a></p>&mdash; Fire &amp; Fuel Apparel (@fire_fuel) <a href="https://twitter.com/fire_fuel/status/781486104545402881">September 29, 2016</a></blockquote>
													<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
												</div>
											</div>
										</div>
										<div class="news-wapper">
											<div class="col-md-12 col-sm-12 col-xs-12" style="width: 225px;">
												<div class="img-news">
													<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FFireAndFuelApparel%2Fposts%2F981036222024320&width=500" width="660" height="250" style="border:none;overflow:hidden; margin-left: 20px;" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					<!--News-->
				</div>
			</div>
		</section>
		
				<?php include '/includes/footer.php'; ?>


		<!--To Top-->
		<div id="copyright">
			<div class="container">
			
				<div class="back-to-top"><a title="BACK_TO_TOP" href="#top"><i class="fa fa-chevron-up"></i></a></div>

				<div class="clrDiv"></div>
			</div>
		</div>
		<!--End To Top-->
	</div>
	<script type="text/javascript" src="js/jquery-2.1.0.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="js/parallax/jquery.parallax-1.1.3.js"></script>
    <script type="text/javascript" src="js/parallax/jquery.transform2d.js"></script>
    <script type="text/javascript" src="js/parallax/script.js"></script>
    <script type="text/javascript" src="js/parallax/parallax.js"></script>
	<script type="text/javascript" src="js/waypoints.js"></script>
	<script type="text/javascript" src="js/template.js"></script>
	<script type="text/javascript" src="js/masterslider.min.js"></script>
	<script type="text/javascript" src="js/banner.js"></script>
	<script type="text/javascript" src="js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="js/theme.js"></script>
	<script type="text/javascript" src="js/dropdown.js"></script>
	<script type="text/javascript" src="js/classie.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
</body>
</html>
