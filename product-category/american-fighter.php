<?php 
	
	{
	include '../functions.php';
	
	$products = get_products();
    
        if(isset($_GET['product']))
    {
        $product = get_products(array('att_str_where' => " AND products.alias = '" . $_GET['product'] . "'"));
    
            $productname = $product[0]['name'];
            $productid = $product[0]['id'];
    
     
    if ($productid > 1) { 
        
        $productid = $productname;     
    }  
    
    }
    
    else {
        $productname = 'Shop';
    }
?>	

<!doctype html>
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en"><!--<![endif]-->
<head>
<title>Mens Clothing</title>
<!--meta info-->
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
<meta name="author" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<link rel="stylesheet" href="../css/style.css" type="text/css">
<link rel="stylesheet" href="../css/bootstrap.css" type="text/css">
<link rel="stylesheet" href="../css/font-awesome.css" type="text/css">
<!--[if !IE 9]><!-->
<link rel="stylesheet" href="../css/effect.css" type="text/css">
<link rel="stylesheet" href="../css/animation.css" type="text/css">
<!--<![endif]-->
<link rel="stylesheet" href="../css/masterslider.css" type="text/css">
<!-- MasterSlider Template Style -->
<link rel="stylesheet" href="../css/ms-tabs-style.css" type="text/css">
<link rel="stylesheet" href="../css/jquery.custombox.css" type="text/css">
<link rel="stylesheet" type="text/css" media="all" href="../css/owl.carousel.css">
<link rel="stylesheet" type="text/css" media="all" href="../css/owl.transitions.css">
<link rel="stylesheet" href="../css/color.css" type="text/css">
</head> 
<body id="page-top" class="" data-offset="90" data-target=".navigation" data-spy="scroll">
	<div class="wrapper hide-main-content"> 
		<section  class="page shop one-page">
			
			<?php include '../includes/nav.php'; ?>
			
				<!--Banner-->
				<section class="page-heading">
					<div class="title-slide">
						<div class="container">
								<div class="banner-content slide-container">									
									<div class="page-title">
										<h3>Mens Clothing</h3>
									</div>
								</div>
						</div>
					</div>
				</section>
				<!--End Banner-->
				<div class="page-content">					
					<!-- Breadcrumbs -->
					<div class="breadcrumbs">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									 <ul>
										 <li class="home"><a href="#"><i class="fa fa-home"></i> Home</a></li>
										 <li><span>//</span></li>
										 <li class="category-2">Mens Clothing</li>
									 </ul>
								</div>
							</div>
                        </div>		
					</div>
					<!-- End Breadcrumbs -->
					
					<!-- Athlete Class -->
					<section id="classes" class="classes">						
						<div class="container">						
							<div class="classes-athlete">
								<div class="classes-inner choose">
									<div class="classes-content">									
										<div class="container">
											<div class="row">
												<div class="col-md-12 col-sm-12 col-xs-12">
													<div class="block-left">
														<div class="about-desc">
															<p>We offer amazing collection of firefighter T-shirts. Our wide variety firefighter T-shirts would simply coax you to buy one from us. We at Fire and Fuel Apparel believe in offering the top notch quality fire fighter T-shirts thus when it comes to the quality aspect, we certainly are the reliable ones. Basically our selection covers a lot of wonderful styles and colors ranging in sizes from small to XL.  And this certainly makes us the absolutely apt choice for people of all sizes.</p>
															<p>Our designs are certainly impressive enough to be the deserving buy.  Whether it is the Firefighter Beast T-shirt or the Firefighter Eagle T-shirt or for that the dead head t-shirt, you can get it all here at Fire and fuel apparel. Our sole aim is to offer you the best and we make sure to stay committed to it through our offerings of flawless collection of firefighter T-shirts.</p>
															<p>Comfort is what we guarantee you on the firefighter T-shirts that we offer. You can certainly be rest assured that by wearing our firefighter T-shirts you would feel the comfort at its best. Needless to say these T-shirts can offer you an absolutely smart appearance.</p>
															<h4 class="about-details-title">Take your pick now! Fire and Fuel Apparel indeed offers the best...</h4>
															<p>The men’s collection feature uniquely designed off-duty firefighter apparel made from high quality apparel and printed here in the U.S.A.!</p>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					
					<?php }
			
					// Get products 
				    // --------------------------------
			        
				    	if(isset($_GET['product']))
						{
					        $product = get_products(array('att_str_where' => " AND products.alias = '" . $_GET['product'] . "'"));
					
					
					        $num_products = count($products); 
						
					?>
		
		
		<!--Banner-->
                    <section class="page-heading">
                        <div class="title-slide">
                            <div class="container">
                                <div class="banner-content">									
                                    <div class="page-title">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--End Banner-->
                    <div class="page-content">					
                        <!-- Breadcrumbs -->
                        <div class="breadcrumbs">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-9">
                                        <ul>
                                            <li class="home"><a href="/"><i class="fa fa-home"></i> Home</a></li>
                                            <li><span>//</span></li>
                                            <li class="home"><a href="/shop">Shop</a></li>
                                            <li><span>//</span></li>
                                            <li class="category-2"><?php echo $product[0]['name']; ?></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Breadcrumbs -->
                        <div class="product-detail">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="product-view">
                                            <div class="product-essential col-md-12" style="background-color: white; color: black;">
                                                <div class="product-img-box col-md-4">
                                                    <a href="#"><img alt="" src="../assets/images/products/<?php echo $product[0]['image']; ?>"></a>
                                                    <div class="more-views">
                                                        <div id="owl-demo" class="owl-carousel owl-theme">
                                                            <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                            <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                            <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                            <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                            <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                            <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                            <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                            <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                            <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                            <div class="item"><img alt="" src="images/newproducts/product-view-more.jpg"></div>
                                                        </div>
                                                        <!--  <a class="jcarousel-control-prev" href="#"><i class="fa fa-caret-left"></i></a>
                                                         <a class="jcarousel-control-next" href="#"><i class="fa fa-caret-right"></i></a> -->
                                                        <div class="customNavigation">
                                                            <a class="btn prev"><i class="fa fa-caret-left"></i></a>
                                                            <a class="btn next"><i class="fa fa-caret-right"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="product-shop col-md-8">
                                                    <div class="product-name">
                                                        <h1><?php echo $product[0]['name']; ?></h1>
                                                    </div>
                                                    <div class="meta-box">
                                                        <div class="price-box">																										
                                                            <span class="special-price"><?php echo $product[0]['price']; ?></span>
                                                        </div>                                                    
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="add-to-box">
                                                                                                                                                                          
													<?php echo $product[0]['shopify']; ?>
                                                 
                                                    </div>
                                                    <div class="cat-list">
                                                        <label>Categories</label><span>:</span>
                                                        <ul>
                                                            <li><?php echo $product[0]['category']; ?></li>
                                                        </ul>
                                                    </div>
                                                    <div class="tags-list">
                                                        <label>Tags</label><span>:</span>
                                                        <ul>
                                                            <li><?php echo $product[0]['tags']; ?></li>
                                                        </ul>
                                                    </div>
                                                    <div class="social-icon">
                                                        <ul>
                                                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="tabs" class="product-collateral">
                                                <ul>
                                                    <li><a href="#tabs-1">Description</a></li>
													<!-- <li><a href="#tabs-2">Reviews</a></li> -->
                                                </ul>
                                                <div id="tabs-1" class="box-collateral">
                                                    <p><?php echo $product[0]['description']; ?></p>
                                                </div>
                                            </div>                                      
                                    	</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>
             	
             	<?php }
    
    
        
    // All products 
    // --------------------------------
    else
    {
	
	?>				
			
						<!-- End Breadcrumbs -->
						<div class="product-list">
							<div class="container">
								<div class="row">
									<div class="col-md-12">
								
										<div class="row">
											
											<?php
												function filterArray($products){
												    return ($products['category'] == "mens");
												}
												$filteredArray = array_filter($products, 'filterArray');
												
									            foreach($filteredArray as $i => $v)
														{
													
											?>

											
											<div class="col-md-3 col-sm-6 col-xs-12">
												<div class="product-image-wrapper">
													<div class="product-content">
														<div class="product-image shop-products">
															<a href="/shop?product=<?php echo $v['alias']; ?>"><img alt="" class="product-img" src="../assets/images/products/<?php echo $v['image']; ?>"></a>
														</div>
														<div class="info-products">
															<div class="product-list-name" >
																<a href="product-detail.html"><?php echo $v['name']; ?></a>
																<div class="product-bottom"></div>
															</div>
															<div class="price-box">																										
																<span class="special-price"><?php echo $v['price']; ?></span>
															</div>
															<div class="actions">
																<ul>																	
																	<li><a href="/shop?product=<?php echo $v['alias']; ?>"><i class="fa fa-shopping-cart"></i></a></li>
																	<li><a href="#"><i class="fa fa-heart"></i></a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
											</div>
									

											<?php
												 
												}
											?>

										</div>
									</div>
								</div>
							</div>
						</div>
						
            
            
        <?php } ?>
					
					
					
					
					
					<!--Popular Products-->
<!--
						<div class="popular-store image-bg-1 scroll-to">
							<div class="container">
								<div class="row">
									<div class="col-md-12">
										<div class="popular-title">
											<h3 class="text-title">Popular Products</h3>
											<div class="next">
												<button class="circle icon-wrap t-nav-prev">
													<i class="fa fa-chevron-left"></i>
												</button>
												<button class="circle icon-wrap t-nav-next">
													<i class="fa fa-chevron-right"></i>
												</button>
											</div>
										</div>
									</div>
									<div id="owl-popular" class="owl-carousel col-md-12" data-nav="t-nav-" data-plugin-options='{"autoPlay":false,"autoHeight":true}'>
										<div class="popular-product">
											<div class="product-image-wrapper">
												<div class="product-content">
													<div class="product-image">
														<a href="#"><img alt="" src="assets/images/mens-shirt-6.jpg"></a>
													</div>
													<div class="info-products">
														<div class="product-name" >
															<a href="#">Octane Deluxe Console</a>
															<div class="product-bottom"></div>
														</div>
														<div class="price-box">																										
															<span class="special-price">$200.60</span>
														</div>
														<div class="actions">
															<ul>
																<li><a href="#"><i class="fa fa-info"></i></a></li>
																<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
																<li><a href="#"><i class="fa fa-search"></i></a></li>
																<li><a href="#"><i class="fa fa-star"></i></a></li>
															</ul>
														</div>
													</div>
												</div>
												<a class="arrows quickview" href="#quickview"><i class="fa fa-arrows"></i></a>										
											</div>											
										</div>
										<div class="popular-product">
											<div class="product-image-wrapper">
												<div class="product-content">
													<div class="product-image">
														<a href="#"><img alt="" src="assets/images/mens-shirt-1.jpg"></a>
													</div>
													<div class="info-products">
														<div class="product-name" >
															<a href="#">Octane Deluxe Console</a>
															<div class="product-bottom"></div>
														</div>
														<div class="price-box">																										
															<span class="special-price">$200.60</span>
														</div>
														<div class="actions">
															<ul>
																<li><a href="#"><i class="fa fa-info"></i></a></li>
																<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
																<li><a href="#"><i class="fa fa-search"></i></a></li>
																<li><a href="#"><i class="fa fa-star"></i></a></li>
															</ul>
														</div>
													</div>
												</div>
												<a class="arrows quickview" href="#quickview"><i class="fa fa-arrows"></i></a>										
											</div>
										</div>
										<div class="popular-product">
											<div class="product-image-wrapper">
												<div class="product-content">
													<div class="product-image">
														<a href="#"><img alt="" src="assets/images/mens-shirt-2.jpg"></a>
													</div>
													<div class="info-products">
														<div class="product-name" >
															<a href="#">Octane Deluxe Console</a>
															<div class="product-bottom"></div>
														</div>
														<div class="price-box">																										
															<span class="special-price">$200.60</span>
														</div>
														<div class="actions">
															<ul>
																<li><a href="#"><i class="fa fa-info"></i></a></li>
																<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
																<li><a href="#"><i class="fa fa-search"></i></a></li>
																<li><a href="#"><i class="fa fa-star"></i></a></li>
															</ul>
														</div>
													</div>
												</div>
												<a class="arrows quickview" href="#quickview"><i class="fa fa-arrows"></i></a>										
											</div>
										</div>
										<div class="popular-product">
											<div class="product-image-wrapper">
												<div class="product-content">
													<div class="product-image">
														<a href="#"><img alt="" src="assets/images/mens-shirt-3.jpg"></a>
													</div>
													<div class="info-products">
														<div class="product-name" >
															<a href="#">Octane Deluxe Console</a>
															<div class="product-bottom"></div>
														</div>
														<div class="price-box">																										
															<span class="special-price">$200.60</span>
														</div>
														<div class="actions">
															<ul>
																<li><a href="#"><i class="fa fa-info"></i></a></li>
																<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
																<li><a href="#"><i class="fa fa-search"></i></a></li>
																<li><a href="#"><i class="fa fa-star"></i></a></li>
															</ul>
														</div>
													</div>
												</div>
												<a class="arrows quickview" href="#quickview"><i class="fa fa-arrows"></i></a>										
											</div>
										</div>
										<div class="popular-product">
											<div class="product-image-wrapper">
												<div class="product-content">
													<div class="product-image">
														<a href="#"><img alt="" src="assets/images/mens-shirt-4.jpg"></a>
													</div>
													<div class="info-products">
														<div class="product-name" >
															<a href="#">Octane Deluxe Console</a>
															<div class="product-bottom"></div>
														</div>
														<div class="price-box">																										
															<span class="special-price">$200.60</span>
														</div>
														<div class="actions">
															<ul>
																<li><a href="#"><i class="fa fa-info"></i></a></li>
																<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
																<li><a href="#"><i class="fa fa-search"></i></a></li>
																<li><a href="#"><i class="fa fa-star"></i></a></li>
															</ul>
														</div>
													</div>
												</div>
												<a class="arrows quickview" href="#quickview"><i class="fa fa-arrows"></i></a>										
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
-->
					<!--End Popular Products-->
					
					
					<?php include '../includes/social-icons.php'; ?>
					
					<?php include '../includes/footer.php'; ?>
					
											
		
		<!--To Top-->
		<div id="copyright">
			<div class="container">

				<div class="back-to-top"><a title="BACK_TO_TOP" href="#top"><i class="fa fa-chevron-up"></i></a></div>

				<div class="clrDiv"></div>
			</div>
		</div>
		<!--End To Top-->
	</div>
	<script type="text/javascript" src="../js/jquery-2.1.0.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/jquery.min.js"></script>
	<script type="text/javascript" src="../js/jquery-ui.js"></script>
	<script type="text/javascript" src="../js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="../js/waypoints.js"></script>
	<script type="text/javascript" src="../../js/template.js"></script>
	<script type="text/javascript" src="../js/masterslider.min.js"></script>
	<script type="text/javascript" src="../js/banner.js"></script>
	<script type="text/javascript" src="../js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="../js/theme.js"></script>
	<script type="text/javascript" src="../js/dropdown.js"></script>
	<script type="text/javascript" src="../js/jquery.custombox.js"></script>
	<script type="text/javascript" src="../js/classie.js"></script>
	<script type="text/javascript" src="../js/main.js"></script>
	<script type="text/javascript">		
	
		var slider = new MasterSlider();

		slider.control('arrows');	
		slider.control('circletimer' , {color:"#FFFFFF" , stroke:9});
		slider.control('thumblist' , {autohide:false ,dir:'h', type:'tabs',width:360,height:120, align:'bottom', space:0 , margin:-12, hideUnder:400});

		slider.setup('masterslider-tab' , {
			width:1366,
			height:768,
			space:0,
			preload:'all',
			view:'basic',
			layout:'fullscreen'
		});
	</script>
	<script>
		$("#owl-popular").owlCarousel({
			items : 4,
			itemsDesktop : [1199,4],
			itemsDesktopSmall : [979,3],
			itemsTablet : [768,2],
			itemsMobile : [479,1],
			lazyLoad : true,
			navigation : false,
			pagination: false,
			autoPlay: false		
		});
				
	</script>
	<script type="text/javascript">
            $(function() {
                $('.quickview').on('click', function(e) {
                    $.fn.custombox(this);
                    var owl = $("#owl-demo-box");
                    owl.owlCarousel({
                        items: 5,
                        itemsDesktop: [1000, 5],
                        itemsDesktopSmall: [900, 3],
                        itemsTablet: [600, 2],
                        itemsMobile: false,
						pagination: false
                    });
                    $(".next").click(function() {
                        owl.trigger('owl.next');
                    })
                    $(".prev").click(function() {
                        owl.trigger('owl.prev');
                    })
                    e.preventDefault();
                });               
            });          
            function increaseQty(id) {
                var qtya = $('.qty-' + id).val();
                var qtyb = qtya * 1 + 1;
                $('.qty-' + id).val(qtyb);
            }
            function decreaseQty(id) {
                var qtya = $('.qty-' + id).val();
                var qtyb = qtya * 1 - 1;
                if (qtyb < 1) {
                    qtyb = 1;
                }
                $('.qty-' + id).val(qtyb);
            }
    </script>
</html>
