<!doctype html>
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en"><!--<![endif]-->
<head>
<title>Fitness Clothing</title>
<!--meta info-->
<meta charset="utf-8">
<meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
<meta name="author" content="">
<meta name="keywords" content="">
<meta name="description" content="">
<link rel="stylesheet" href="../css/style.css" type="text/css">
<link rel="stylesheet" href="../css/bootstrap.css" type="text/css">
<link rel="stylesheet" href="../css/font-awesome.css" type="text/css">
<!--[if !IE 9]><!-->
<link rel="stylesheet" href="../css/effect.css" type="text/css">
<link rel="stylesheet" href="../css/animation.css" type="text/css">
<!--<![endif]-->
<link rel="stylesheet" href="../css/masterslider.css" type="text/css">
<link rel="stylesheet" href="../css/color.css" type="text/css"> 
</head> 
<body id="page-top" class="" data-offset="90" data-target=".navigation" data-spy="scroll">
	<div class="wrapper hide-main-content"> 
		<section  class="page shop one-page">
			
			<?php include '../includes/nav.php'; ?>
			
				<!--Banner-->
				<section class="page-heading">
					<div class="title-slide">
						<div class="container">
								<div class="banner-content slide-container">									
									<div class="page-title">
										<h3>Fitness Clothing</h3>
									</div>
								</div>
						</div>
					</div>
				</section>
				<!--End Banner-->
				<div class="page-content">					
					<!-- Breadcrumbs -->
					<div class="breadcrumbs">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									 <ul>
										 <li class="home"><a href="#"><i class="fa fa-home"></i> Home</a></li>
										 <li><span>//</span></li>
										 <li class="category-2">Fitness Clothing</li>
									 </ul>
								</div>
							</div>
                        </div>		
					</div>
					<!-- End Breadcrumbs -->
					
					<!-- Athlete Class -->
					<section id="classes" class="classes">						
						<div class="container">						
							<div class="classes-athlete">
								<div class="classes-inner choose">
									<div class="classes-content">									
										<div class="container">
											<div class="row">
												<div class="col-md-12 col-sm-12 col-xs-12">
													<div class="block-left">
														<div class="about-title">
															<h3>Fire and Fuel Fitness Apparrel</h3>
														</div>
														<div class="about-desc">
															<p>Fire and Fuel Apparel brings a unique selection of stylish and comfortable firefighter fitness clothing option that offers flexibility and comfort during physical activities. These uniquely designed fitness clothing are specifically meant for the demanding lifestyle of the firefighters and first responders.</p>
															<p>Explore our collection of firefighter fitness apparels for men and women in the theme that stands for bravery, patriotism, sport and life-saving attitude.</p>
															<h4 class="about-details-title">Clothing for All Occasions</h4>
															<p>Whether you want to work, relax or workout, our firefighter fitness apparels keeps you comfortable throughout. These are the specially designed apparels for firefighters who want to wear their pride wherever they go. You can wear them comfortably for any outdoor or indoor physical activities. They are designed for both men and women firefighters and for anyone who want to show their support, love and appreciation for the heroes and the work they do.</p>										
														</div>
													</div>
												</div>
												<br />
												<div class="col-md-6 col-sm-12 col-xs-12">
													<div class="block-left">
														<h4 class="about-details-title">Our Unique collection Includes:</h4>
															<ul>
																<li>American Firefighter T-shirts and Real Heroes Without Capes design</li>
																<li>Cross-fit and MMA-style athletic gear</li>
																<li>T-shirts, V-neck, long sleeve shirts, hoodies, tank-tops for women</li>
																<li>Camouflage hunting apparel and many more.</li>
															</ul>
															<br />
														<div class="about-title">
															<h4>Best Designs Firefighter Fitness Clothing</h4>
														</div>
														<div class="about-desc">
															<p>At Fire and Fuel Apparel, we are passionate about firefighters work and all our clothing and accessories and gears reflect the passion through the designs. For firefighters and first responders, our uniquely designed fitness clothing provides the best way to wear their pride and show their love for what they do. Our firefighter fitness clothing come in various firefighter themes, designs and prints and quotes and striking colour to add to the aura.</p>
															<h4 class="about-details-title">Best Quality Firefighter Fitness Apparel</h4>
															<p>We aim to provide high quality firefighter fitness clothing and gear that reflects the spirit of hardworking of fire service personnel. All the apparels are made of strong, breathable and quality materials that can withstand the pressure of the firefighter’s lifestyle. They are equally comfortable and relaxing to provide all-round comfort for the daring firefighters in a physically demanding career.</p>
														</div>
													</div>
												</div>
												<div class="col-md-6 col-sm-12 col-xs-12">
													<div class="block-right">
														<br />
														<div class="about-img">
															<img src="assets/images/Firefighter-Maltese-Warrior-Black-Shorts.jpg" alt=""/>
														</div>
													</div>
												</div>																																																		
											</div>
										</div>																				
									</div>
								</div>
							</div>
						</div>
					</section>
					<!-- End Athlete Class -->
					<section id="classes" class="classes">						
						<div class="container">						
							<div class="classes-athlete">
								<div class="classes-inner choose">
									<div class="classes-content">									
										<div class="container">
											<div class="row">
												<div class="col-md-12 col-sm-12 col-xs-12">
													<div style="text-align: center;">										
														<div class="about-desc">
															<p><b>Start browsing now. Call us at (801) 376-7124 for any queries. Else please contact us at support@fireandfuelapparel.com with any questions you might have about our products or services we offer.</b></p>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					
					<?php include '../includes/social-icons.php'; ?>
					
		<!--Footer-->
		<footer class="page-footer">
			<section>
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-12 infomation">
							<div class="copy-right">
								<div class="footer-right">
									<div class="footer-title">
										<h4>Sitemap</h4>
										<ul class="footer-list">
											<li class="footer-links">Home</li>
											<li class="footer-links">Our Inspiration</li>
											<li class="footer-links">Shop</li>
											<li class="footer-links">Fitness Clothing</li>
											<li class="footer-links">Roll Call</li>
											<li class="footer-links">Gifts</li>
											<li class="footer-links">Social</li>
											<li class="footer-links">Contact Us</li>
										</ul>			
									</div>
									<div class="line1">Copyright &copy; 2016 Fire and Fuel Apparrel</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12 location">							
							<div class="footer-title">
								<h4>Product Categories</h4>
								<ul class="footer-list">
									<li class="footer-links">Accessories</li>
									<li class="footer-links">Base Layer</li>
									<li class="footer-links">Fitness Clothing</li>
									<li class="footer-links">Gear</li>
									<li class="footer-links">Hats</li>
									<li class="footer-links">Kids</li>
									<li class="footer-links">Mens</li>
									<li class="footer-links">On Point Supplements</li>
								</ul>				
							</div>	
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12 send-mail">							
							<div class="fb-page" data-href="https://www.facebook.com/FireAndFuelApparel/?fref=ts" data-tabs="timeline" data-width="350" data-height="250" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/FireAndFuelApparel/?fref=ts" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/FireAndFuelApparel/?fref=ts">Fire And Fuel Apparel</a></blockquote></div>
						</div>						
					</div>
				</div>
			</section>
		</footer>
		<!--End Footer-->
		<!--To Top-->
		<div id="copyright">
			<div class="container">

				<div class="back-to-top"><a title="BACK_TO_TOP" href="#top"><i class="fa fa-chevron-up"></i></a></div>

				<div class="clrDiv"></div>
			</div>
		</div>
		<!--End To Top-->
	</div>
	<script type="text/javascript" src="../js/jquery-2.1.0.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" src="../js/shortcode-frontend.js"></script>
	<script type="text/javascript" src="../js/jquery.mixitup.js"></script>	
	<script type="text/javascript" src="../js/parallax/jquery.parallax-1.1.3.js"></script>
    <script type="text/javascript" src="../js/parallax/jquery.transform2d.js"></script>
    <script type="text/javascript" src="../js/parallax/script.js"></script>
    <script type="text/javascript" src="../js/parallax/parallax.js"></script>
	<script type="text/javascript" src="../js/jquery.scrollTo.js"></script>
	<script type="text/javascript" src="../js/wow.min.js"></script>
	<script type="text/javascript" src="../js/waypoints.js"></script>		
	<script type="text/javascript" src="../js/template.js"></script>
	<script type="text/javascript" src="../js/classie.js"></script>
	<script type="text/javascript" src="../js/dropdown.js"></script>
	<script type="text/javascript" src="../js/custom.js"></script>
	<script type="text/javascript" src="../js/main.js"></script>    
</html>
